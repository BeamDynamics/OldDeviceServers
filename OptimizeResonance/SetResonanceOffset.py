import tango
import matplotlib.pyplot as plt

skewdevice = 'srmag/sqp/all'
resdevices = 'sr/reson-skew/m1_n-1_p49', 'sr/reson-skew/m1_n1_p104'

s = tango.DeviceProxy(skewdevice)
skewvals = s.Strengths

for id, resdevname in enumerate(resdevices):
    dev = tango.DeviceProxy(resdevname)
    dev.Offset = skewvals

fig, ax = plt.subplots()
ax.plot(skewvals,color='r')
