# -*- coding: utf-8 -*-
#
# This file is part of the OptimizeResonance project
#
# Copyright (C): 2017
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" optimizer for resonances

Optimize Resonances to minimize emittance, maximize lifetime,...
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(OptimizeResonance.additionnal_import) ENABLED START #

import optimize_amplitude_phase
import time
from threading import Timer, Lock
from beam_test import sr_test
from numpy import pi
from functools import partial

# PROTECTED REGION END #    //  OptimizeResonance.additionnal_import

__all__ = ["OptimizeResonance", "main"]


class OptimizeResonance(Device):
    """
    Optimize Resonances to minimize emittance, maximize lifetime,...
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(OptimizeResonance.class_variable) ENABLED START #

    def starttime(self):
        """defines the next time to run an optimization.
        to mimic the behaviour of zemloop, run at the given minute of every hour. """

        # may not act on struct_time tuple, so convert to list.
        starttimestruct = list(self.now)

        # zeroth second to the minute
        starttimestruct[5] = int(0)

        optstartmin = int(self.optimize_at_minute)

        # set hour
        if int(starttimestruct[4]) > optstartmin:
            # start at minute of next hour
            starttimestruct[3] = starttimestruct[3] + int(1)

        # set minute
        starttimestruct[4] = optstartmin

        t = time.mktime(starttimestruct)

        return t

    def start_timer_thread(self):
        """Start a thread for optimization with a given time delay
        """

        # get current time
        tn = time.localtime()
        sttime0 = time.mktime(tn)

        # get starting time
        sttime = self.starttime()

        print('If ON, Next optimization will run at: {} if {}'.format(
            time.ctime(sttime),self.condstring))

        self.set_status('If ON, Next optimization will run at: {} if {}'.format(
            time.ctime(sttime),self.condstring))

        # store next starting time for display in status upon request
        self.nextstarttime = sttime

        # start an optimization with the computed time delay from self.now
        tmr=Timer(sttime - sttime0, self.optimize_resonance_forever)

        # in case server is killed the thread is killed too
        tmr.isDaemon()
        tmr.start()

        # append to list of running timers to cancel upon OFF command
        self.runningtimers.append(tmr)

        return

    def perform_optimization(self,testusm = True, testemittance = True):
        """ performs the resonance optimization, first phase, than amplitude.
        runs optimization only if no other optimization is running. also checks if there is beam in the SR and is USM mode
        testusm (default=True): flag to check if beam is ok, used for example """

        #  do not run if an other optimization is running
        if not self.lockrunning.locked():

            # initialize test beam to test ok (False, binary error message from sr_test, 0=no error)
            testbeampassed = False

            # check do not run if the beam has been lost or not in USM

            # sr_test is ebs/esrf dependent. set up property for current, lifetime, orbit and USM and test here
            try:

                # ESRF sr_test
                testbeampassed = sr_test(
                    current=[0.5, 260],
                    lifetime=[60, 7200000],
                    emittance_v=[0, 50],
                    orbit=True,
                    usm=testusm)

            except Exception:

                # EBS sr_test
                testbeampassed = sr_test(
                    current=[0.5, 260],
                    lifetime=[60, 7200000],
                    emittance_v=[0, 50],
                    orbit=False,
                    usm=False,  # false SYS/MACHSTAT/TANGO/SR_MODE not running
                    verbose=False,
                    current_attribute='srdiag/beam-current/total/Current',
                    lifetime_attribute='srdiag/beam-current/total/Lifetime',
                    emittance_vertical_attribute='srdiag/emittance/id25/Emittance_v',
                    orbit_device='sr/d-orbit/1',  # the d-orbit device still needs to be created for EBS
                    usm_attribute='SYS/MACHSTAT/TANGO/SR_MODE')


            if not testbeampassed: # go on if there is beam

                # test emittance threshold
                testemittancepassed = True

                emit = optimize_amplitude_phase.vertical_emittance_average_in_time(
                    devattribute=self.attribute_to_minimize_name)
                print('Emittance is {} nm'.format(emit))
                print('Emittance threshold is {} nm'.format(self.emittance_level))

                if testemittance:
                    testemittancepassed = emit[0] > self.emittance_level

                # perform optimization only if emittance above specified minimum value [nm]
                if testemittancepassed:
                    # lock
                    self.lockrunning.acquire()

                    # get initial state (could be On or Off)
                    initialstate = self.get_state()

                    self.set_state(DevState.RUNNING)
                    self.set_status('Performing optimization')

                    # get initial phase [deg] and amplitude [], return error if devices not imported correctly and kill server, thus.
                    with optimize_amplitude_phase.trycatcherr('Could not read amplitude device',error=True):
                        self.amplini = optimize_amplitude_phase.get_phase_or_ampl(
                            self.resonance_device_name + '/Amplitude')

                    with optimize_amplitude_phase.trycatcherr('Could not read phase device',error=True):
                        self.phaseini = optimize_amplitude_phase.get_phase_or_ampl(
                            self.resonance_device_name + '/Phase') / pi * 180

                    print('Initial Amplitude is: {}'.format(self.amplini))

                    # check that amplitude is non zero, if zero, set to small amplitude and zero phase

                    start_ampl = self.amplini
                    start_phase = self.phaseini

                    if self.amplini < 0.0001:
                        start_ampl = 0.01
                        start_phase = 0.0
                        msg = 'amplitude of {} is too small, setting ({}A, {}deg) to ({}A, {}deg)'.format(
                            str(self.resonance_device_name),
                            str(self.amplini), str(self.phaseini),
                            str(start_ampl), str(start_phase))
                        print(msg)

                        self.set_status(msg)
                        # self.amplini = start_ampl
                        # self.phaseini = start_phase

                        optimize_amplitude_phase.set_phase_or_ampl(
                            self.resonance_device_name + '/Amplitude', start_ampl)

                        # self.Reset()
                        time.sleep(1) # sleep here for amplitude device

                    msg = 'Minimizing {} with resonance {}'.format(
                        str(self.attribute_to_minimize_name), str(self.resonance_device_name))

                    print(msg)
                    self.set_status(msg)

                    try:
                        # Look for best phase
                        print('find optimum phase')
                        self.set_status('find optimum phase')

                        vact, _ , _ = optimize_amplitude_phase.phaseloop(
                            self.resonance_device_name + '/Phase',
                            start_phase / 180 * pi, # input phase in rad
                            self.phase_step_val,
                            self.min_emit_var,
                            optimize_amplitude_phase.vertical_emittance_average_in_time,
                            self.attribute_to_minimize_name)

                        # save optimal phase (if Drift of emittance during amplitude loop, phase optimization is kept).
                        self.phaseini = vact / pi *180

                        print('Optimal phase. Set {} to: {}, {} deg'.format(
                            self.resonance_device_name,
                            start_ampl,
                            start_phase))

                        self.set_status('Optimal phase. Set {} to: {}, {} deg'.format(
                            self.resonance_device_name,
                            start_ampl,
                            start_phase))

                        # Look for best amplitude
                        print('find optimum amplitude')
                        self.set_status('find optimum amplitude')

                        optimize_amplitude_phase.amplloop(
                            self.resonance_device_name + '/Amplitude',
                            start_ampl,
                            self.amplitude_step_val,
                            self.max_amplitude_val,
                            self.min_emit_var,
                            optimize_amplitude_phase.vertical_emittance_average_in_time,
                            self.attribute_to_minimize_name)

                        # send message if everything worked fine
                        msg = 'optimization finished with success. If ON, the next optimization will run on: {} if {}'.format(
                            time.ctime(self.nextstarttime),self.condstring)

                        self.set_status(msg)

                        # unlock
                        self.lockrunning.release()  # release lock so that an other optimization can be started

                    except ValueError as err:
                        # this error is rised if after optimization the check of the emittance fails (something moved meanwhile)
                        self.errors = err
                        print('Drift during optimization. Keep optimum values.')
                        self.set_status('Drift during optimization. Keep optimum values.')
                        time.sleep(1) # sleep here for debugging
                        self.Reset()
                        # unlock
                        self.lockrunning.release()  # release lock so that an other optimization can be started

                    except Exception as err:

                        # unlock
                        self.lockrunning.release()  # release lock so that an other optimization can be started
                        self.errors = err
                        print('Error during optimization')
                        self.set_status('Error during optimization')
                        self.set_state(DevState.FAULT)
                        self.Reset()
                        raise

                    # set state of device back to original one (ON or OFF)
                    self.set_state(initialstate)

                else:
                    # emittance above limit test failed
                    msg='Emittance {} nm already below accepted value {} nm'.format(emit[0], self.emittance_level)
                    print(msg)
                    self.set_status(msg)

            else:
                # sr_test, no beam, or lifetime to low, or orbit bad or
                mesgintepreted=''

                errorbin = "{0:05b}".format(testbeampassed)

                if errorbin[0] is '1':
                    mesgintepreted = mesgintepreted + 'emittance '
                if errorbin[1] is '1':
                    mesgintepreted = mesgintepreted + 'USM '
                if errorbin[2] is '1':
                    mesgintepreted = mesgintepreted + 'orbit '
                if errorbin[3] is '1':
                    mesgintepreted = mesgintepreted + 'Current '
                if errorbin[4] is '1':
                    mesgintepreted = mesgintepreted + 'lifetime '

                msg='sr_test failed, optimization skipped with error message {} : {}'.format(testbeampassed, mesgintepreted)
                print(msg)
                self.set_status(msg)

        else:
            # optimizator thread is locked, something is already happening!
            msg='An other optimization is already running at this time.'
            print(msg)
            self.set_status(msg)

    def optimize_resonance_forever(self):
        """
        after running a resonance optimization schedules the next one
        :return:
        """

        # if ON, do optimization, if not ON just schedule next timer and exit
        if self.get_state() == DevState.ON:
            # do the optimization
            self.perform_optimization()

        # wait 30s before scheduling next (2 attempts per minute)
        # if no waiting and at given minute the device is off or the optimization is not performed, the scheduling will
        # occur continuously untill the minute ends.
        time.sleep(30)

        # schedule next timer
        print('starting the next thread')
        # define starting date
        self.now = time.localtime()
        # start next thread
        self.start_timer_thread()

        return

    # PROTECTED REGION END #    //  OptimizeResonance.class_variable

    # -----------------
    # Device Properties
    # -----------------

    resonance_device_name = device_property(
        dtype='str', default_value="sr/reson-sext/m0_n3_p229"
    )

    attribute_to_minimize_name = device_property(
        dtype='str', default_value="srdiag/emittance/id25/Emittance_v"
    )

    optimize_at_minute = device_property(
        dtype='str', default_value="25"
    )

    # ----------
    # Attributes
    # ----------

    minimum_emittance_variation = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        unit="pm",
        memorized=True,
    )

    max_amplitude = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        memorized=True,
    )

    amplitude_step = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        memorized=True,
    )

    phase_step = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        unit="deg",
        memorized=True,
    )

    emittance_threshold = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(OptimizeResonance.init_device) ENABLED START #

        # set defaults
        self.min_emit_var = 0.01 *1e-12  #[m] emittance significance threshold
        self.amplitude_step_val = 0.01 # amplitude step
        self.max_amplitude_val = 0.15 # amplitude max.
        self.phase_step_val = 10.0  # deg. phase step
        self.emittance_level = 8.5 *1e-3 # below this emittance the optimization does not take place [nm]
        self.now = time.localtime() # date and time at which On has been pressed.
        self.nextstarttime = 0.0 # no next time scheduled to run optimization
        self.lockrunning=Lock() # lock to check if som other optimization is already running
        self.runningtimers =[] # list of running timers to cancel them on object destruction
        self.condstring = " USM,\n 0.5mA < current < 260mA,\n 60s < lifetime < 2000h,\n  0pm < emit_v < 50pm,\n there is an orbit "

        print('optimize: {}'.format(self.attribute_to_minimize_name))
        print('using: {}'.format(self.resonance_device_name))
        msg='optimize {} using {}'.format(
            self.attribute_to_minimize_name,
            self.resonance_device_name
            )
        self.set_status(msg)

        # get initial phase [deg] and amplitude [], return error if devices not imported correctly and kill server, thus.
        with optimize_amplitude_phase.trycatcherr('Could not read amplitude device',error=True):
            self.amplini = optimize_amplitude_phase.get_phase_or_ampl(
                self.resonance_device_name + '/Amplitude')

        with optimize_amplitude_phase.trycatcherr('Could not read phase device',error=True):
            self.phaseini = optimize_amplitude_phase.get_phase_or_ampl(
                self.resonance_device_name + '/Phase') / pi * 180

        print('Initial amplitude: {} '.format(self.amplini))
        print('Initial phase    : {} deg'.format(self.phaseini) )

        # start timer, if device is off it will just keep scheduling new optimization without doing anything.
        self.start_timer_thread()

        # error handling
        self.errors = ''

        # state to OFF, no recursive optimization scheduled
        self.set_state(DevState.OFF)

        # PROTECTED REGION END #    //  OptimizeResonance.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(OptimizeResonance.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  OptimizeResonance.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(OptimizeResonance.delete_device) ENABLED START #
        # cancel all timers
        [t.cancel() for t in self.runningtimers]
        # PROTECTED REGION END #    //  OptimizeResonance.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_minimum_emittance_variation(self):
        # PROTECTED REGION ID(OptimizeResonance.minimum_emittance_variation_read) ENABLED START #
        return self.min_emit_var*1e12 # convert to pm
        # PROTECTED REGION END #    //  OptimizeResonance.minimum_emittance_variation_read

    def write_minimum_emittance_variation(self, value):
        # PROTECTED REGION ID(OptimizeResonance.minimum_emittance_variation_write) ENABLED START #
        self.min_emit_var = value*1e-12 # convert to m
        # PROTECTED REGION END #    //  OptimizeResonance.minimum_emittance_variation_write

    def read_max_amplitude(self):
        # PROTECTED REGION ID(OptimizeResonance.max_amplitude_read) ENABLED START #
        return self.max_amplitude_val
        # PROTECTED REGION END #    //  OptimizeResonance.max_amplitude_read

    def write_max_amplitude(self, value):
        # PROTECTED REGION ID(OptimizeResonance.max_amplitude_write) ENABLED START #
        self.max_amplitude_val = value
        # PROTECTED REGION END #    //  OptimizeResonance.max_amplitude_write

    def is_max_amplitude_allowed(self, attr):
        # PROTECTED REGION ID(OptimizeResonance.is_max_amplitude_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_max_amplitude_allowed

    def read_amplitude_step(self):
        # PROTECTED REGION ID(OptimizeResonance.amplitude_step_read) ENABLED START #
        return self.amplitude_step_val
        # PROTECTED REGION END #    //  OptimizeResonance.amplitude_step_read

    def write_amplitude_step(self, value):
        # PROTECTED REGION ID(OptimizeResonance.amplitude_step_write) ENABLED START #
        if value < self.max_amplitude_val and value >0:
            self.amplitude_step_val = value
        else:
            self.set_status('amplitude step must between 0 and maximum amplitude')
            print('amplitude step must between 0 and maximum amplitude')
        # PROTECTED REGION END #    //  OptimizeResonance.amplitude_step_write

    def is_amplitude_step_allowed(self, attr):
        # PROTECTED REGION ID(OptimizeResonance.is_amplitude_step_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_amplitude_step_allowed

    def read_phase_step(self):
        # PROTECTED REGION ID(OptimizeResonance.phase_step_read) ENABLED START #
        return self.phase_step_val
        # PROTECTED REGION END #    //  OptimizeResonance.phase_step_read

    def write_phase_step(self, value):
        # PROTECTED REGION ID(OptimizeResonance.phase_step_write) ENABLED START #
        if value < 180 and value > -180:
            self.phase_step_val = value
        else:
            print('phase step must be between -180 and 180 deg')
        # PROTECTED REGION END #    //  OptimizeResonance.phase_step_write

    def read_emittance_threshold(self):
        # PROTECTED REGION ID(OptimizeResonance.emittance_threshold_read) ENABLED START #
        return self.emittance_level*1e3 # convert to pm
        # PROTECTED REGION END #    //  OptimizeResonance.emittance_threshold_read

    def write_emittance_threshold(self, value):
        # PROTECTED REGION ID(OptimizeResonance.emittance_threshold_write) ENABLED START #
        self.emittance_level = value*1e-3 # convert to nm
        # PROTECTED REGION END #    //  OptimizeResonance.emittance_threshold_write

    def is_emittance_threshold_allowed(self, attr):
        # PROTECTED REGION ID(OptimizeResonance.is_emittance_threshold_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_emittance_threshold_allowed


    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(OptimizeResonance.On) ENABLED START #

        # set state On
        self.set_state(DevState.ON)

        return
        # PROTECTED REGION END #    //  OptimizeResonance.On

    def is_On_allowed(self):
        # PROTECTED REGION ID(OptimizeResonance.is_On_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON,DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_On_allowed

    @command(
    )
    @DebugIt()
    def Force(self):
        # PROTECTED REGION ID(OptimizeResonance.Force) ENABLED START #
        """ force optimization now (if no other optimization is running)"""
        self.set_status('In 1 second Force will run an optimization. Be patient.')

        # start am optimization Timer (not a thread, to execute .cancel() on all running timers on delete of the object)
        perf_opt_now_nocheck = partial(self.perform_optimization,testusm=False, testemittance=False)
        tmr = Timer(1, perf_opt_now_nocheck)

        tmr.isDaemon()# in case server is killed the thread is killed too
        tmr.start()

        # append to list of running timers to cancel upon OFF command
        self.runningtimers.append(tmr)

        # PROTECTED REGION END #    //  OptimizeResonance.Force

    def is_Force_allowed(self):
        # PROTECTED REGION ID(OptimizeResonance.is_Force_allowed) ENABLED START #
        return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_Force_allowed

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(OptimizeResonance.Reset) ENABLED START #
        """
         re-set initial resonance phase and amplitude

        :return:
        """

        print('Going back to initial phase and amplitude')

        with optimize_amplitude_phase.trycatcherr('Could not set amplitude device'):
            optimize_amplitude_phase.set_phase_or_ampl(
                self.resonance_device_name + '/Amplitude',
                self.amplini)

        with optimize_amplitude_phase.trycatcherr('Could not set phase device'):
            optimize_amplitude_phase.set_phase_or_ampl(
                self.resonance_device_name + '/Phase',
                self.phaseini / 180 * pi)

        print('Set {} to: {}, {} deg'.format(
            self.resonance_device_name,
            self.amplini,
            self.phaseini))

        self.set_status('Set {} to: {}, {} deg'.format(
            self.resonance_device_name,
            self.amplini,
            self.phaseini))

        pass
        # PROTECTED REGION END #    //  OptimizeResonance.Reset

    def is_Reset_allowed(self):
        # PROTECTED REGION ID(OptimizeResonance.is_Reset_allowed) ENABLED START #
        return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_Reset_allowed

    @command(
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(OptimizeResonance.Off) ENABLED START #

        self.set_state(DevState.OFF)

        self.set_status('Device is OFF, no optimization will occur automatically')

        # PROTECTED REGION END #    //  OptimizeResonance.Off

    def is_Off_allowed(self):
        # PROTECTED REGION ID(OptimizeResonance.is_Off_allowed) ENABLED START #
        return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  OptimizeResonance.is_Off_allowed

    @command(
    )
    @DebugIt()
    def WhenNext(self):
        # PROTECTED REGION ID(OptimizeResonance.WhenNext) ENABLED START #
        """prints on status the next starting time scheduled"""

        if not self.nextstarttime == 0:
            self.set_status('If ON, Next optimization will run at: {} if {}'.format(
                time.ctime(self.nextstarttime),self.condstring))
        else:
            self.set_status('no scheduled optimization')

        # PROTECTED REGION END #    //  OptimizeResonance.WhenNext

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(OptimizeResonance.main) ENABLED START #
    return run((OptimizeResonance,), args=args, **kwargs)
    # PROTECTED REGION END #    //  OptimizeResonance.main

if __name__ == '__main__':
    main()
