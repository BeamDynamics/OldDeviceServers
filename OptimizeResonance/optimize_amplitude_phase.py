from __future__ import print_function           # for python2
from builtins import range, str
from pymach.tango import Device, Attribute
from numpy import array, mean, pi, nan, nanmean, nanstd, arange
from contextlib import contextmanager
import time
import math
from tango import DevSource


@contextmanager
def trycatcherr(msg='Could not contact Device',error=False):
    try:
        yield
    except StandardError:
        print(msg)
        if error:
            raise


def degrange(vdeg, step):
    vv=arange(vdeg+step, vdeg+180+step, step)
    vv = [v-360 if v > 180 else v for v in vv]
    return array(vv)


def phaseloop(phaseattribute, phaseini, phasestep, nstd, measfunc,*args, **kwargs):
    """
    find optimum phase (in deg)
    """

    phaseinideg = 180 * phaseini / pi

    vp =  degrange(phaseinideg, phasestep)
    vm =  -degrange(-phaseinideg, phasestep)
    # print('testing phases: {} and {} deg'.format(vp ,vm ))
    vact, vbest, moved = paramloop(phaseattribute, nstd, vp / 180 * pi, vm / 180 * pi, measfunc,*args, **kwargs)

    return vact, vbest, moved


def amplloop(amplattribute, amplini, amplstep, amplmax, nstd, measfunc,*args, **kwargs):
    """
    find optimum amplitude
    """
    vp =  arange(amplini + amplstep, amplmax + amplstep, amplstep)
    vm =  arange(amplini - amplstep,   - amplstep      , -amplstep)

    vact, vbest, moved = paramloop(amplattribute,nstd, vp, vm, measfunc,*args, **kwargs)

    return vact, vbest, moved

def testfun(dummydev = (999, 0),nave=5, period=1):
    '''
        compute average of dummydev(0)*cos(dummydev(1)) over nave samples every period

    :param dummydev:
    :param nave:
    :param period:
    :return:
    '''

    em = [nan for i in range(0, nave)]

    # average in time
    for i in range(0, nave):
        em[i] = dummydev[0]*math.cos(dummydev[1])
        time.sleep(period)

    emz = nanmean(em)
    errez = nanstd(em)

    return emz, errez

def vertical_emittance_average_in_time(devattribute='srdiag/emittance/id25/Emittance_v',
                   nave=5, period=1):
    """

    compute average of Emittance_z for given device name
    over nave samples every period

    :param devname:
    :param nave:
    :param period:
    :return:
    """
    # average emittance (over several measurement device device

    emattr = Attribute(devattribute,source = DevSource.DEV) # use source DEV to get data directly from device.

    em = [nan for i in range(0, nave)]

    # average in time
    for i in range(0, nave):
        em[i] = emattr.value
        time.sleep(period)

    ev = nanmean(em)
    errev = nanstd(em)

    return ev, errev

def get_phase_or_ampl(devattr):
    """get set-point of attribute within context manager"""

    v=nan

    with trycatcherr('Could not read Device ' + devattr):
        attr=Attribute(devattr,source = DevSource.DEV)# use source DEV to get data directly from device.
        v = attr.setpoint

    return v

def set_phase_or_ampl(devattr,v):
    """assign set-point of attribute within context manager"""

    with trycatcherr('Could not set Device ' + devattr):
        attr = Attribute(devattr)
        attr.setpoint = v

    return

def paramloop(devact, nstd, vp, vm, measfunc,*args, **kwargs):
    """
    PARAMLOOP	Looks for the minimum value of a parameter

    [VACT,VBEST,MOVED]=PARAMLOOP(DEV,MEASFUNC,NSTD,VP,VM)

    DEV:      device attribute name
    MEASFUNC: function returning the value to be minimized, as [val,errval]=MEASFUNC;
              VAL: measured value, ERRVAL: accuracy of the measurement (std. dev.)
    NSTD:     if numerical: fixed significance threshold
              if cell array: NSTD{1} Number of std deviations for the significance threshold
                             NSTD{2} nb o measurements for threshold determination
    VP:	   array of values to be tested in the first direction
    VM:	   array of values to be tested in the other direction

    VACT:	Best value for the variable
    VBEST:	Corresponding minimum parameter value
    MOVED:	logical indicating if the best result is different from the initial value

    """
    setpause = 6

    if type(nstd) is tuple:
        nini = 3
        if len(nstd) >= 2:
            nini = nstd[1]

        vv = nan((nini, 1,))
        errvv = nan(nini, 1)

        for i in range(0, nini):
            vv[i], errvv[i] = measfunc(*args, **kwargs)

        vini = mean(vv)
        threshold = nstd[0] * mean(errvv)
    else:
        vini ,errvv = measfunc(*args, **kwargs)
        threshold = nstd # value of minimum acceptable change, if none, optimization stops.

    print(threshold)

    vactini = get_phase_or_ampl(devact)

    print('Initial {:g} -> {:g}, ({:g},-{:g})'.format(vactini, vini, vini - threshold, vini + threshold))

    vact = vactini  # actual device attribute value
    vbest = vini  # initial reading

    moved = False

    # try first direction
    for vnext in vp:
        # set correction
        set_phase_or_ampl(devact,vnext)

        # wait correctors setting
        time.sleep(setpause)

        # measure result
        vv, bid = measfunc(*args, **kwargs)

        bad = (vbest - vv) < threshold
        print('up {:g} -> {:g} ({:d})\n'.format(vnext, vv, bad))
        # stop if no improvement
        if bad:
            break
        vact = vnext
        vbest = vv
        moved = True

    # if it fails try other direction
    if not moved:
        # try second direction
        for vnext in vm:
            # set correction
            set_phase_or_ampl(devact, vnext)

            # wait correctors
            time.sleep(setpause)

            # measure result
            vv, bid = measfunc(*args, **kwargs)

            bad = (vbest - vv) < threshold
            print('down {:g} -> {:g} ({:d})\n'.format(vnext, vv, bad))
            # stop if no improvement
            if bad:
                break
            vact = vnext
            vbest = vv
            moved = True

    # check if anything changed during optimization
    if moved:
        # set correction
        set_phase_or_ampl(devact,vactini)

        # wait correctors
        time.sleep(1.5 * setpause)

        # measure result
        vv, bid = measfunc(*args, **kwargs)

        # check improvement compared to initial
        bad = vbest > (vv - threshold)
        print('check {:g} -> {:g} with initial {:g} ({:d})\n'.format(vactini, vv,vbest, bad))
        if bad:
            raise ValueError

    # set final correction
    set_phase_or_ampl(devact, vact)

    print('final {:g}\n'.format(vact))

    return vact, vbest, moved
