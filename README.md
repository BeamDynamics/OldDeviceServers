Devices Servers from the Beam Dynamics group

- InterpolBump.py : compute the injection bump by interpolation into a kicker table
- OptimizeResonance.py : optimize emittance using amplitude and phase of resonances at given minute in the hour or on-demand