# -*- coding: utf-8 -*-
#
# This file is part of the RCDSOptimization project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" 

allows optimization a given attribute objective 
with a limited number of attributes as variables.
Use X.Huang (SLAC) R.C.D.S. (Robust conjugate Direction Search)
"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import device_property
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(RCDSOptimization.additionnal_import) ENABLED START #
from rcds import *
from pymach.tango import Attribute
import time
import matplotlib.pyplot as plt
# PROTECTED REGION END #    //  RCDSOptimization.additionnal_import

__all__ = ["RCDSOptimization", "main"]


class RCDSOptimization(Device):
    """
    allows optimization a given attribute objective 
    with a limited number of attributes as variables.
    Use X.Huang (SLAC) R.C.D.S. (Robust conjugate Direction Search)
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(RCDSOptimization.class_variable) ENABLED START #

    def func_obj(self,x):
        '''Objective function : vertical emittance
        Input:
            x : a column vector of skew quadrupoles strengths
        Output:
            obj : vertical emittance
        '''
        global g_cnt, g_data, g_vrange
        global g_noise
        Nvar = len(x)
        # print(x)
        # print(g_vrange[:,0])
        p = g_vrange[:, 0] + np.multiply((g_vrange[:, 1] - g_vrange[:, 0]), x)
        # print(p)

        # respect limits
        if min(x) < 0 or max(x) > 1:
            obj = float('NaN')
            return obj

        maxstep = self.maxstep()  # maxmum step allowed
        # current settings
        pact = []
        for dev in self.devnames():
            cur = Attribute(dev).setpoint
            pact.append(cur)

        self.set_state(DevState.RUNNING)

        # set skew quadrupoles strengths
        for dev, ip, ipa, ms in zip(self.devnames(), p, pact,maxstep):
            attr = Attribute(dev)
            print('{} at {}A'.format(dev, ip))
            self.set_status = '{} at {}A'.format(dev, ip)
            # split in steps less than given amplitude
            if abs(ip - ipa) > ms:
                NP = abs(ip - ipa) // ms
                print('large excursion from {} to {}. Sharing in {} steps'.format(ipa, ip, NP))
                self.set_status = 'large excursion from {} to {}. Sharing in {} steps'.format(ipa, ip, NP)
                ptemp = ipa
                stepsize = (ip - ipa) / NP
                # apply in steps
                for i in range(1, NP + 1):
                    ptemp = ptemp + stepsize
                    print('step {} / {} : {}'.format(i,NP,ptemp))
                    time.sleep(0.5)

                    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
                    # # # # # # COMMENT BELOW TO lock action on magnets ! # # # # # #
                    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
                    try:
                        attr.setpoint = ptemp
                    except Exception:
                        print('Could not set attribute. May be out of range?')
                        self.set_status = 'Could not set attribute. May be out of range?'

                print('arrived')

            else:
                try:
                    attr.setpoint = ip
                except Exception:
                    print('Could not set attribute. May be out of range?')
                    self.set_status = 'Could not set attribute. May be out of range?'

        # wait some time
        print('wait for {} s'.format(self.waitPS))
        time.sleep(self.waitPS)

        # get vertical emittance value (no averaging)
        try:

            obj = Attribute(self.Objective).value

        except Exception as inst:
            # print(type(inst) )    # the exception instance
            # print(inst.args )     # arguments stored in .args
            # print(inst     )      # __str__ allows args to be printed directly
            obj = None

        if obj is None:
            self.set_state(DevState.FAULT)
            self.set_status = 'Objective could not be computed'
            return

        # change signe for maximization
        if not(self.MinOrMax):
            obj = - obj

        print('measured {} = {}'.format(self.Objective,obj))
        g_cnt += 1
        print(g_cnt)
        # print(np.asarray(p).reshape(-1))
        # print(obj)
        dentry = np.asarray(np.r_[g_cnt, np.asarray(p).reshape(-1), obj])
        print(dentry)
        if g_cnt <= g_data.shape[0]:
            g_data[g_cnt - 1, :] = dentry
        else:
            g_data = np.concatenate((g_data, [dentry]), axis=0)
        print(g_data.shape)

        # # figure
        # plt.subplot(211)
        # plt.plot(g_data[:, 0], g_data[:, 1:-1])
        # plt.xlabel('cnt')
        # plt.ylabel('magnet strengths')
        # plt.grid(True)
        #
        # plt.subplot(212)
        # plt.plot(g_data[:, 0], g_data[:, -1], 'r--')
        # plt.xlabel('cnt')
        # plt.ylabel('objective')
        # plt.grid(True)
        # plt.show(block=False)
        # plt.pause(0.2)

        return obj

    def devnames(self):
        return [x.strip() for x in self.Attributes.split(',')]

    def min(self):
        return [float(x.strip()) for x in self.Min.split(',')]

    def max(self):
        return [float(x.strip()) for x in self.Max.split(',')]

    def maxstep(self):
        return [float(x.strip()) for x in self.MaxStep.split(',')]

    # PROTECTED REGION END #    //  RCDSOptimization.class_variable

    # -----------------
    # Device Properties
    # -----------------

    Attributes = device_property(
        dtype='str', default_value="srmag/sqp-sf2/C03-e/Current, srmag/sqp-sf2/C04-a/Current"
    )

    Max = device_property(
        dtype='str', default_value="2,2"
    )

    Min = device_property(
        dtype='str', default_value="-2,-2"
    )

    Objective = device_property(
        dtype='str', default_value="srdiag/emittance/id25/Emittance_v"
    )

    MaxStep = device_property(
        dtype='str', default_value="0.01,20"
    )

    # ----------
    # Attributes
    # ----------

    Maxiterations = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    WaitforPS = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    MinimizeOrMaximize = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(RCDSOptimization.init_device) ENABLED START #
        self.waitPS = 4 # seconds
        self.maxiter = 5
        self.MinOrMax = True # true = minimize, false = maximize

        self.set_state(DevState.ON)

        # PROTECTED REGION END #    //  RCDSOptimization.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(RCDSOptimization.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  RCDSOptimization.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(RCDSOptimization.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  RCDSOptimization.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_Maxiterations(self):
        # PROTECTED REGION ID(RCDSOptimization.Maxiterations_read) ENABLED START #
        return self.maxiter
        # PROTECTED REGION END #    //  RCDSOptimization.Maxiterations_read

    def write_Maxiterations(self, value):
        # PROTECTED REGION ID(RCDSOptimization.Maxiterations_write) ENABLED START #
        self.maxiter=value
        pass
        # PROTECTED REGION END #    //  RCDSOptimization.Maxiterations_write

    def is_Maxiterations_allowed(self, attr):
        # PROTECTED REGION ID(RCDSOptimization.is_Maxiterations_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  RCDSOptimization.is_Maxiterations_allowed

    def read_WaitforPS(self):
        # PROTECTED REGION ID(RCDSOptimization.WaitforPS_read) ENABLED START #
        return self.waitPS
        # PROTECTED REGION END #    //  RCDSOptimization.WaitforPS_read

    def write_WaitforPS(self, value):
        # PROTECTED REGION ID(RCDSOptimization.WaitforPS_write) ENABLED START #
        self.waitPS = value
        pass
        # PROTECTED REGION END #    //  RCDSOptimization.WaitforPS_write

    def is_WaitforPS_allowed(self, attr):
        # PROTECTED REGION ID(RCDSOptimization.is_WaitforPS_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  RCDSOptimization.is_WaitforPS_allowed

    def read_MinimizeOrMaximize(self):
        # PROTECTED REGION ID(RCDSOptimization.MinimizeOrMaximize_read) ENABLED START #
        return self.MinOrMax
        # PROTECTED REGION END #    //  RCDSOptimization.MinimizeOrMaximize_read

    def write_MinimizeOrMaximize(self, value):
        # PROTECTED REGION ID(RCDSOptimization.MinimizeOrMaximize_write) ENABLED START #
        self.MinOrMax = value
        if value is True:
            self.set_status('Look for Minimum')
        else:
            self.set_status('Look for Maximum')

        pass
        # PROTECTED REGION END #    //  RCDSOptimization.MinimizeOrMaximize_write

    def is_MinimizeOrMaximize_allowed(self, attr):
        # PROTECTED REGION ID(RCDSOptimization.is_MinimizeOrMaximize_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  RCDSOptimization.is_MinimizeOrMaximize_allowed


    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def Start(self):
        # PROTECTED REGION ID(RCDSOptimization.Start) ENABLED START #

        # define global variables
        global gnoise, g_cnt, g_vrange, g_data
        g_noise = 2.8596e-04
        g_cnt = 0

        Nvar = len(self.devnames())
        print('optimize {} variables'.format(Nvar))

        print(self.devnames())
        if Nvar == 0:
            self.set_state(DevState.ALLARM)
            self.set_status = 'no device to optimize'
            pass
        else:
            self.set_state(DevState.RUNNING)
            self.set_status = 'optimize {} devices'.format(Nvar)

        # plt.figure(1)

        # define range of skew strengths
        g_vrange = np.matrix([self.min(),self.max()]).transpose()

        g_data = np.zeros([1, Nvar + 2])

        Imat = np.matrix(np.identity(Nvar))

        # get initial skew quadrupole currents
        cur = []

        for dev in self.devnames():
            c = Attribute(dev).setpoint
            cur.append(c)

        print(cur)
        # save currents
        self.bestcur = cur

        p0 = np.matrix(cur).transpose()  # column vector of skew quadrupole strengths

        # normalize to range (0,1)
        x0 = np.divide(p0 - g_vrange[:, 0], g_vrange[:, 1] - g_vrange[:, 0])

        # take normalized skew strneghts ranges (0,1) and outputs the corresponding measured vertical emittance
        y0 = self.func_obj(x0)

        print('initial objective: {}'.format(y0))
        step = 0.1
        (xm, fm, nf) = powellmain(self.func_obj, x0, step, Imat,maxIt=self.maxiter)

        # plt.show(block=False)

        print([x0, xm])
        print(y0, fm)

        for ii in range(g_cnt):
            print(g_data[ii, :])

        self.set_state(DevState.ON)
        self.set_status = 'Optimization terminated'

        pass
        # PROTECTED REGION END #    //  RCDSOptimization.Start

    def is_Start_allowed(self):
        # PROTECTED REGION ID(RCDSOptimization.is_Start_allowed) ENABLED START #
        return self.get_state() not in [DevState.FAULT,DevState.RUNNING]
        # PROTECTED REGION END #    //  RCDSOptimization.is_Start_allowed

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(RCDSOptimization.main) ENABLED START #
    # util = PyTango.Util.instance()
    # util.set_serial_mode = PyTango.SerialModel.NO_SYNC
    return run((RCDSOptimization,), args=args, **kwargs)
    # PROTECTED REGION END #    //  RCDSOptimization.main

if __name__ == '__main__':
    main()
