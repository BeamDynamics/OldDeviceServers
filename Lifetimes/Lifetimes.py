# -*- coding: utf-8 -*-
#
# This file is part of the Lifetimes project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Lifetimes

"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import device_property
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(Lifetimes.additionnal_import) ENABLED START #
import math
# PROTECTED REGION END #    //  Lifetimes.additionnal_import

__all__ = ["Lifetimes", "main"]


class Lifetimes(Device):
    """
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(Lifetimes.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  Lifetimes.class_variable

    # -----------------
    # Device Properties
    # -----------------

    CTsinglebunch = device_property(
        dtype='str', default_value="sr/d-ct/ict1"
    )

    CTmultibunch = device_property(
        dtype='str', default_value="sr/d-ct/1"
    )

    Liberalifetimeserver = device_property(
        dtype='str', default_value="sr/d-bpmlibera/lifetime"
    )

    Filling_mode_server = device_property(
        dtype='str', default_value="sys/machstat/tango"
    )

    Average_emittance_server = device_property(
        dtype='str', default_value="sr/d-emit/average"
    )

    # ----------
    # Attributes
    # ----------

    Filling_mode = attribute(
        dtype='str',
        label="Filling mode",
    )

    Normalize_train_LT = attribute(
        dtype='double',
        display_level=DispLevel.EXPERT,
        label="Normalized train Touschek LT",
        unit="h",
        doc="Touschek lifetime of the train normalized to the value at 'reference train current' mA, at 'reference vertical emittance' pmrad and with given vacuum lifetime.",
    )

    Reference_vertical_emittance = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        label="Reference vertical emittance",
        unit="pm rad",
    )

    Single_Bunch_current = attribute(
        dtype='double',
        label="Single Bunch current",
        unit="mA",
    )

    Single_Bunch_lifetime = attribute(
        dtype='double',
        label="Single Bunch lifetime",
        unit="h",
    )

    Total_current = attribute(
        dtype='double',
        label="Total current",
        unit="mA",
    )

    Total_lifetime = attribute(
        dtype='double',
        label="Total lifetime",
        unit="h",
    )

    Train_current = attribute(
        dtype='double',
        label="Train Current",
        unit="mA",
    )

    Train_lifetime = attribute(
        dtype='double',
        label="Train lifetime",
        unit="h",
    )

    Use_Libera_lifetime = attribute(
        dtype='bool',
        access=AttrWriteType.READ_WRITE,
        label="Use Libera lifetime",
    )

    Vertical_emittance = attribute(
        dtype='double',
        display_level=DispLevel.EXPERT,
        label="Vertical emittance",
        unit="pm rad",
    )

    Reference_train_current = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        label="Reference train current",
        unit="mA",
    )

    Vacuumlifetime = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
        label="Vacuum lifetime",
        unit="h",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(Lifetimes.init_device) ENABLED START #
        #self.UseLibera=False
        #self.RefVertEmitt=8
        #self.RefTrCurr=194
        #print('ciao')
        self.write_Use_Libera_lifetime(False)
        self.write_Reference_vertical_emittance(8)
        #print("reference_vertical_emittance = ",self.RefVertEmitt)
        self.write_Reference_train_current(194)
        self.write_Vacuumlifetime(350)
        self.set_status('Ok')
        self.set_state(DevState.ON)
        self.CTSingleDev=PyTango.DeviceProxy(self.CTsinglebunch)
        self.CTTotalDev=PyTango.DeviceProxy(self.CTmultibunch)
        self.LiberaDev=PyTango.DeviceProxy(self.Liberalifetimeserver)
        self.EmitAveDev = PyTango.DeviceProxy(self.Average_emittance_server)
        # PROTECTED REGION END #    //  Lifetimes.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(Lifetimes.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  Lifetimes.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(Lifetimes.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  Lifetimes.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_Filling_mode(self):
        # PROTECTED REGION ID(Lifetimes.Filling_mode_read) ENABLED START #
        FMserver=PyTango.DeviceProxy(self.Filling_mode_server)
        return FMserver.filling_mode
        # PROTECTED REGION END #    //  Lifetimes.Filling_mode_read

    def read_Normalize_train_LT(self):
        # PROTECTED REGION ID(Lifetimes.Normalize_train_LT_read) ENABLED START #
        emv=self.read_Vertical_emittance()
        trlt=self.read_Train_lifetime()
        TouTrLT=1/(1/trlt-1/self.VacLT)
        emv_ref=self.RefVertEmitt
        trCurr=self.read_Train_current()
        trCurr_ref=self.RefTrCurr
        normlt=TouTrLT*math.sqrt(emv_ref/emv)*(trCurr/trCurr_ref)

        return normlt
        # PROTECTED REGION END #    //  Lifetimes.Normalize_train_LT_read

    def read_Reference_vertical_emittance(self):
        # PROTECTED REGION ID(Lifetimes.Reference_vertical_emittance_read) ENABLED START #
        return self.RefVertEmitt
        # PROTECTED REGION END #    //  Lifetimes.Reference_vertical_emittance_read

    def write_Reference_vertical_emittance(self, value):
        # PROTECTED REGION ID(Lifetimes.Reference_vertical_emittance_write) ENABLED START #
        self.RefVertEmitt=value
        # PROTECTED REGION END #    //  Lifetimes.Reference_vertical_emittance_write

    def read_Single_Bunch_current(self):
        # PROTECTED REGION ID(Lifetimes.Single_Bunch_current_read) ENABLED START #
        return self.CTSingleDev.Current
        # PROTECTED REGION END #    //  Lifetimes.Single_Bunch_current_read

    def read_Single_Bunch_lifetime(self):
        # PROTECTED REGION ID(Lifetimes.Single_Bunch_lifetime_read) ENABLED START #
        return self.CTSingleDev.Lifetime/3600
        # PROTECTED REGION END #    //  Lifetimes.Single_Bunch_lifetime_read

    def read_Total_current(self):
        # PROTECTED REGION ID(Lifetimes.Total_current_read) ENABLED START #
        return self.CTTotalDev.Current
        # PROTECTED REGION END #    //  Lifetimes.Total_current_read

    def read_Total_lifetime(self):
        # PROTECTED REGION ID(Lifetimes.Total_lifetime_read) ENABLED START #
        if self.UseLibera:
            return self.LiberaDev.Lifetime/3600
        else:
            return self.CTTotalDev.Lifetime/3600
        # PROTECTED REGION END #    //  Lifetimes.Total_lifetime_read

    def read_Train_current(self):
        # PROTECTED REGION ID(Lifetimes.Train_current_read) ENABLED START #
        return self.read_Total_current()-self.read_Single_Bunch_current()
        # PROTECTED REGION END #    //  Lifetimes.Train_current_read

    def is_Train_current_allowed(self, attr):
        # PROTECTED REGION ID(Lifetimes.is_Train_current_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF]
        # PROTECTED REGION END #    //  Lifetimes.is_Train_current_allowed

    def read_Train_lifetime(self):
        # PROTECTED REGION ID(Lifetimes.Train_lifetime_read) ENABLED START #
        Itr=self.read_Train_current()
        Itot=self.read_Total_current()
        ISB=self.read_Single_Bunch_current()
        ttot=self.read_Total_lifetime()
        tsb=self.read_Single_Bunch_lifetime()
        return Itr/((Itot/ttot)-(ISB/tsb))
        # PROTECTED REGION END #    //  Lifetimes.Train_lifetime_read

    def read_Use_Libera_lifetime(self):
        # PROTECTED REGION ID(Lifetimes.Use_Libera_lifetime_read) ENABLED START #
        return self.UseLibera
        # PROTECTED REGION END #    //  Lifetimes.Use_Libera_lifetime_read

    def write_Use_Libera_lifetime(self, value):
        # PROTECTED REGION ID(Lifetimes.Use_Libera_lifetime_write) ENABLED START #
        self.UseLibera=value
        # PROTECTED REGION END #    //  Lifetimes.Use_Libera_lifetime_write

    def read_Vertical_emittance(self):
        # PROTECTED REGION ID(Lifetimes.Vertical_emittance_read) ENABLED START #
        return self.EmitAveDev.Zemittance*1000
        # PROTECTED REGION END #    //  Lifetimes.Vertical_emittance_read

    def is_Vertical_emittance_allowed(self, attr):
        # PROTECTED REGION ID(Lifetimes.is_Vertical_emittance_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF]
        # PROTECTED REGION END #    //  Lifetimes.is_Vertical_emittance_allowed

    def read_Reference_train_current(self):
        # PROTECTED REGION ID(Lifetimes.Reference_train_current_read) ENABLED START #
        return self.RefTrCurr
        # PROTECTED REGION END #    //  Lifetimes.Reference_train_current_read

    def write_Reference_train_current(self, value):
        # PROTECTED REGION ID(Lifetimes.Reference_train_current_write) ENABLED START #
        self.RefTrCurr=value
        pass
        # PROTECTED REGION END #    //  Lifetimes.Reference_train_current_write

    def read_Vacuumlifetime(self):
        # PROTECTED REGION ID(Lifetimes.Vacuumlifetime_read) ENABLED START #
        return self.VacLT
        # PROTECTED REGION END #    //  Lifetimes.Vacuumlifetime_read

    def write_Vacuumlifetime(self, value):
        # PROTECTED REGION ID(Lifetimes.Vacuumlifetime_write) ENABLED START #
        self.VacLT=value
        # PROTECTED REGION END #    //  Lifetimes.Vacuumlifetime_write


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(Lifetimes.main) ENABLED START #
    return run((Lifetimes,), args=args, **kwargs)
    # PROTECTED REGION END #    //  Lifetimes.main

if __name__ == '__main__':
    main()
