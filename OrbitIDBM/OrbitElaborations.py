# -*- coding: utf-8 -*-
#
# This file is part of the OrbitElaborations project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Elaoration of Orbit readings

reads orbit positions, determines peak and rms, computes beam position and angle at ID and BM sources
"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import device_property
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(OrbitElaborations.additionnal_import) ENABLED START #
from pymach.tango import Attribute
from ebs.model import Model
import numpy as np
import tango
from datetime import datetime
from threading import Timer, Lock

# import sys
# import os
# _rootdir = os.path.join(os.environ['APPHOME'], 'ebs', 'optics', 'settings')
# _opticsdir = os.path.join(_rootdir, 'theory')

# PROTECTED REGION END #    //  OrbitElaborations.additionnal_import

__all__ = ["OrbitElaborations", "main"]


class OrbitElaborations(Device):
    """
    reads orbit positions, determines peak and rms, computes beam position and angle at ID and BM sources
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(OrbitElaborations.class_variable) ENABLED START #
    def readbpms(self):
        self.set_state(DevState.RUNNING)

        # set overall status to the status of the BPM device
        self.set_status(Attribute(self.BpmDevice + '/' + 'Status').value)

        # # get current BPM readings and status (not synchronous!)
        # self.hbpm = Attribute(self.BpmDevice+'/'+self.HAttribute).value
        # self.vbpm = Attribute(self.BpmDevice+'/'+self.VAttribute).value
        # self.sbpm = Attribute(self.BpmDevice+'/'+'All_Status').value # status of bpms

        # get current BPM readings and status (synchronous)
        bpmdev = tango.DeviceProxy(self.BpmDevice)
        synchread = bpmdev.read_attributes(
            [self.HAttribute, self.VAttribute, 'All_Status'])

        self.hbpm = synchread[0].value
        self.vbpm = synchread[1].value
        self.sbpm = synchread[2].value

        # mask for BPMs to use
        self.goodbpm = self.sbpm==0

        # BM positions
        self.bmh = np.array(self.hbpm[self.bmbpm])
        self.bmv = np.array(self.vbpm[self.bmbpm])

        # ID positions and angles
        id_up = np.array(self.hbpm[self.upbpm])
        id_do = np.array(self.hbpm[self.dobpm])

        self.hpos = (id_up + id_do) /2
        self.hang = [np.asscalar((d - u) / l) for d, u, l in zip(list(id_do), id_up, np.array(self.id_sl))]

        id_up = np.array(self.vbpm[self.upbpm])
        id_do = np.array(self.vbpm[self.dobpm])
        self.vpos = (id_up + id_do) /2
        self.vang = [np.asscalar((d - u) / l) for d, u, l in zip(list(id_do), id_up, np.array(self.id_sl))]


        # get reference orbit (no average over previous period)
        t = datetime.now()
        print('computing positions at id at {}'.format(t))

        if self.on_init or \
                (int(t.hour) == 6  and int(t.minute) == 0 ) or \
                (int(t.hour) == 14 and int(t.minute) == 0 ) or \
                (int(t.hour) == 22 and int(t.minute) == 0 ) :

            self.refh = self.hpos
            self.refv = self.vpos
            self.refh_bm = self.bmh
            self.refv_bm = self.bmv
            self.refha = self.hang
            self.refva = self.vang
            self.on_init = False
            self.set_status('updated reference orbit at {}'.format(t))

        # initilize emittance growths to positions
        self.hemg = [0]*len(self.hpos)
        self.vemg = [0]*len(self.hpos)
        self.hemg_bm = [0]*len(self.bmh)
        self.vemg_bm = [0]*len(self.bmv)

        #compute ID emittance growths
        for i in range(1,len(self.hpos)):

            self.hemg[i] = np.asscalar( 2 * np.sqrt( (
                            ((self.hpos[i] - self.refh[i] )**2 ) / self.id_betah[i] +
                             ((self.hang[i] - self.refha[i])**2 ) * self.id_betah[i] )
                            / self.refeh ) )

            self.vemg[i] = np.asscalar( 2 * np.sqrt(
                           ((self.vpos[i] - self.refv[i] )**2 / self.id_betav[i] +
                            (self.vang[i] - self.refva[i])**2 * self.id_betav[i] ) / self.refev ) )

        #compute BM emittance growths
        for i in range(1,len(self.bmh)):

            self.hemg_bm[i] = np.asscalar( 2 * np.sqrt( (
                            ((self.bmh[i] - self.refh_bm[i] )**2 ) / self.bm_betah[i] )
                            / self.refeh ) )

            self.vemg_bm[i] = np.asscalar( 2 * np.sqrt(
                           ((self.bmv[i] - self.refv_bm[i] )**2 / self.bm_betav[i] ) / self.refev ) )


        return
    # PROTECTED REGION END #    //  OrbitElaborations.class_variable

    # -----------------
    # Device Properties
    # -----------------

    HAttribute = device_property(
        dtype='str', default_value="SA_HPositions"
    )

    VAttribute = device_property(
        dtype='str', default_value="SA_VPositions"
    )

    BpmDevice = device_property(
        dtype='str', default_value="srdiag/beam-position/all"
    )

    path_to_lattice_model = device_property(
        dtype='str', default_value="/operation/appdata/ebs/optics/settings/theory"
    )

    # ----------
    # Attributes
    # ----------

    vPeak = attribute(
        dtype='double',
    )

    vSD = attribute(
        dtype='double',
    )

    hPeak = attribute(
        dtype='double',
    )

    hSD = attribute(
        dtype='double',
    )

    vRms = attribute(
        dtype='double',
    )

    hRms = attribute(
        dtype='double',
    )

    Vpos_ID = attribute(
        dtype=('double',),
        max_dim_x=64,
    )

    Hpos_ID = attribute(
        dtype=('double',),
        max_dim_x=64,
    )

    Hang_ID = attribute(
        dtype=('double',),
        max_dim_x=64,
    )

    Vang_ID = attribute(
        dtype=('double',),
        max_dim_x=64,
    )

    Hemg_ID = attribute(
        dtype=('double',),
        max_dim_x=64,
    )

    Vemg_ID = attribute(
        dtype=('double',),
        max_dim_x=64,
    )

    Hpos_BM = attribute(
        dtype=('double',),
        max_dim_x=32,
    )

    Vpos_BM = attribute(
        dtype=('double',),
        max_dim_x=32,
    )

    Hemg_BM = attribute(
        dtype=('double',),
        max_dim_x=32,
    )

    Vemg_BM = attribute(
        dtype=('double',),
        max_dim_x=32,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(OrbitElaborations.init_device) ENABLED START #
        # load devices for simulator and bpms
        self.bpmdev = tango.DeviceProxy(self.BpmDevice)

        # get optics parameters from model optics.
        # (Init on this device will automatically update the values)
        # matlab licence should be released after init ends
        lat = Model(self.path_to_lattice_model)

        # get global lattice parameters
        self.espread = lat.energy_spread
        self.refeh = lat.emith
        self.refev = lat.emitv
        if self.refev < 5e-12:
            print('emittance to 5pm')
            self.refev = 5e-12

        # get optics at IDs and BMs
        self.id_betah = lat.get([3], 'id')
        self.id_alphah = lat.get([4], 'id')
        self.id_etah = lat.get([6], 'id')
        self.id_etaph = lat.get([7], 'id')
        self.id_betav = lat.get([8], 'id')
        self.id_alphav = lat.get([9], 'id')

        bmx = lat.get([0], 'bm')
        self.bm_betah = lat.get([3], 'bm')
        self.bm_alphah = lat.get([4], 'bm')
        self.bm_etah = lat.get([6], 'bm')
        self.bm_etaph = lat.get([7], 'bm')
        self.bm_betav = lat.get([8], 'bm')
        self.bm_alphav = lat.get([9], 'bm')


        # BPM indexes up and down stream of IDs
        bpmind = range(0,320,1)
        self.upbpm = bpmind[-1:] + bpmind[9:311:10]
        self.dobpm = bpmind[0::10]
        self.bmbpm = bpmind[5::10] # BPM 6 at side of BM source

        # get length by getting s position of up and down stream BPMs.
        self.bpm_spos = lat.get([13], 'bpm')

        ups = self.bpm_spos[self.upbpm] # USstream S positions
        dos = self.bpm_spos[self.dobpm] # DOwnstream S positions

        self.id_sl = np.array(dos) - np.array(ups)

        # take care of ID04 (first straigth section)
        self.id_sl[0] = np.array(lat.ll) - np.array(ups[0]) + np.array(dos[0])

        # flag to trigger computation of reference orbit on Init command
        self.on_init = True

       # # start continuous acquisition of BPM data
       # tmr = Timer(1, self.readbpms)
       # tmr.isDaemon()# in case server is killed the thread is killed too
       # tmr.start()
       # self.runningtimers.append(tmr)

        # set device state
        self.set_state(DevState.ON)
        # PROTECTED REGION END #    //  OrbitElaborations.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(OrbitElaborations.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  OrbitElaborations.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(OrbitElaborations.delete_device) ENABLED START #
        # [t.cancel() for t in self.runningtimers]
        pass
        # PROTECTED REGION END #    //  OrbitElaborations.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_vPeak(self):
        # PROTECTED REGION ID(OrbitElaborations.vPeak_read) ENABLED START #
        return max(abs(self.vbpm[self.goodbpm]))
        # PROTECTED REGION END #    //  OrbitElaborations.vPeak_read

    def read_vSD(self):
        # PROTECTED REGION ID(OrbitElaborations.vSD_read) ENABLED START #
        return np.std(self.vbpm[self.goodbpm])
        # PROTECTED REGION END #    //  OrbitElaborations.vSD_read

    def read_hPeak(self):
        # PROTECTED REGION ID(OrbitElaborations.hPeak_read) ENABLED START #
        return max(abs(self.hbpm[self.goodbpm]))
        # PROTECTED REGION END #    //  OrbitElaborations.hPeak_read

    def read_hSD(self):
        # PROTECTED REGION ID(OrbitElaborations.hSD_read) ENABLED START #
        return np.std(self.vbpm[self.goodbpm])
        # PROTECTED REGION END #    //  OrbitElaborations.hSD_read

    def read_vRms(self):
        # PROTECTED REGION ID(OrbitElaborations.vRms_read) ENABLED START #
        return np.std(self.vbpm[self.goodbpm])
        # PROTECTED REGION END #    //  OrbitElaborations.vRms_read

    def read_hRms(self):
        # PROTECTED REGION ID(OrbitElaborations.hRms_read) ENABLED START #
        return np.std(self.hbpm[self.goodbpm])
        # PROTECTED REGION END #    //  OrbitElaborations.hRms_read

    def read_Vpos_ID(self):
        # PROTECTED REGION ID(OrbitElaborations.Vpos_ID_read) ENABLED START #
        return self.vpos
        # PROTECTED REGION END #    //  OrbitElaborations.Vpos_ID_read

    def read_Hpos_ID(self):
        # PROTECTED REGION ID(OrbitElaborations.Hpos_ID_read) ENABLED START #
        return self.hpos
        # PROTECTED REGION END #    //  OrbitElaborations.Hpos_ID_read

    def read_Hang_ID(self):
        # PROTECTED REGION ID(OrbitElaborations.Hang_ID_read) ENABLED START #
        return self.hang
        # PROTECTED REGION END #    //  OrbitElaborations.Hang_ID_read

    def read_Vang_ID(self):
        # PROTECTED REGION ID(OrbitElaborations.Vang_ID_read) ENABLED START #
        return self.vang
        # PROTECTED REGION END #    //  OrbitElaborations.Vang_ID_read

    def read_Hemg_ID(self):
        # PROTECTED REGION ID(OrbitElaborations.Hemg_ID_read) ENABLED START #
        return self.hemg
        # PROTECTED REGION END #    //  OrbitElaborations.Hemg_ID_read

    def read_Vemg_ID(self):
        # PROTECTED REGION ID(OrbitElaborations.Vemg_ID_read) ENABLED START #
        return self.vemg
        # PROTECTED REGION END #    //  OrbitElaborations.Vemg_ID_read

    def read_Hpos_BM(self):
        # PROTECTED REGION ID(OrbitElaborations.Hpos_BM_read) ENABLED START #
        return self.bmh
        # PROTECTED REGION END #    //  OrbitElaborations.Hpos_BM_read

    def read_Vpos_BM(self):
        # PROTECTED REGION ID(OrbitElaborations.Vpos_BM_read) ENABLED START #
        return self.bmv
        # PROTECTED REGION END #    //  OrbitElaborations.Vpos_BM_read

    def read_Hemg_BM(self):
        # PROTECTED REGION ID(OrbitElaborations.Hemg_BM_read) ENABLED START #
        return self.hemg_bm
        # PROTECTED REGION END #    //  OrbitElaborations.Hemg_BM_read

    def read_Vemg_BM(self):
        # PROTECTED REGION ID(OrbitElaborations.Vemg_BM_read) ENABLED START #
        return self.vemg_bm
        # PROTECTED REGION END #    //  OrbitElaborations.Vemg_BM_read


    # --------
    # Commands
    # --------

    @DebugIt()
    def dev_state(self):
        # PROTECTED REGION ID(OrbitElaborations.State) ENABLED START #
        Device.dev_state(self)
        t = datetime.now()
        print('computing positions at id at {}'.format(t))
        self.readbpms()
        return self.get_state()
        # PROTECTED REGION END #    //  OrbitElaborations.State

    @command(
    dtype_in='DevEnum', 
    doc_in="source_number", 
    dtype_out=('double',), 
    )
    @DebugIt()
    def GetOpticalParams(self, argin):
        # PROTECTED REGION ID(OrbitElaborations.GetOpticalParams) ENABLED START #
        return [0.0]
        # PROTECTED REGION END #    //  OrbitElaborations.GetOpticalParams

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(OrbitElaborations.main) ENABLED START #
    return run((OrbitElaborations,), args=args, **kwargs)
    # PROTECTED REGION END #    //  OrbitElaborations.main

if __name__ == '__main__':
    main()
