# -*- coding: utf-8 -*-
#
# This file is part of the InjKickerDummy project
#
# Copyright (C): 2018
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" dummy device to pilot injection kickers

dummy injection kickers device
"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(InjKickerDummy.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  InjKickerDummy.additionnal_import

__all__ = ["InjKickerDummy", "main"]


class InjKickerDummy(Device):
    """
    dummy injection kickers device
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(InjKickerDummy.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  InjKickerDummy.class_variable

    # ----------
    # Attributes
    # ----------

    Currents = attribute(
        dtype=('double',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=4,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(InjKickerDummy.init_device) ENABLED START #
        # PROTECTED REGION END #    //  InjKickerDummy.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(InjKickerDummy.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(InjKickerDummy.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_Currents(self):
        # PROTECTED REGION ID(InjKickerDummy.Currents_read) ENABLED START #
        return [0.0]
        # PROTECTED REGION END #    //  InjKickerDummy.Currents_read

    def write_Currents(self, value):
        # PROTECTED REGION ID(InjKickerDummy.Currents_write) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.Currents_write


    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(InjKickerDummy.On) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.On

    @command(
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(InjKickerDummy.Off) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.Off

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(InjKickerDummy.Reset) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.Reset

    @command(
    )
    @DebugIt()
    def Standby(self):
        # PROTECTED REGION ID(InjKickerDummy.Standby) ENABLED START #
        pass
        # PROTECTED REGION END #    //  InjKickerDummy.Standby

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(InjKickerDummy.main) ENABLED START #
    return run((InjKickerDummy,), args=args, **kwargs)
    # PROTECTED REGION END #    //  InjKickerDummy.main

if __name__ == '__main__':
    main()
