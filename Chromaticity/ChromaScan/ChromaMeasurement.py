# -*- coding: utf-8 -*-
#
# This file is part of the ChromaMeasurement project
#
# Copyright (C): 2019
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Chromaticity Measurement Server

A server to automatically calculate the Chromaticity to allow for easy correction during operation.
"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import class_property, device_property
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(ChromaMeasurement.additionnal_import) ENABLED START #
import numpy as np
import time, datetime
from functools import partial
from threading import Timer, Lock, Event, Thread
from scipy.optimize import curve_fit
# PROTECTED REGION END #    //  ChromaMeasurement.additionnal_import

__all__ = ["ChromaMeasurement", "main"]


class ChromaMeasurement(Device):
    """
    A server to automatically calculate the Chromaticity to allow for easy correction during operation.
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(ChromaMeasurement.class_variable) ENABLED START #

    def dp2freq(self, dp):
        '''
        Convert a number in units of Delta(dp)
        to Delta(f)
        '''
        dF = dp*self.freq_2_p
        return dF

    def getTune(self,device,plane):
        '''
        For a given device and plane, return the
        desired tune value.
        '''
        if 'h' in plane or 'H' in plane:
            return device.read_attribute('Qh').value
        elif 'v' in plane or 'V' in plane:
            return device.read_attribute('Qv').value

    def storeRFFrequency(self,device):
        '''
        The main RF frequency used by the code for dp=0. 
        '''
        self.storeRF = device.read_attribute('Frequency').value/1e6

    def setFrequency(self,device, dp):
        '''
        Function used to calculate and write the new
        frequency value for each dp step
        '''
        dF = self.dp2freq(dp)
        newFreq = self.storeRF*1e6*(1 + dF)
        device.write_attribute('Frequency', newFreq)


    def makeDPArray(self):
        '''
        Take all the numbers and make an array
        of dp values
        '''
        low = self.read_MinDP()*1e-4
        high = self.read_MaxDP()*1e-4
        n = self.read_NumSteps()
        self.DP_s = np.linspace(low,high,n)

    def curve(self, dp, Q, Qp, Qpp):
        return Q + Qp*dp + 0.5*Qpp*dp*dp

    def doFit(self):

        meanH = np.mean(self.allTunesH, axis=1)
        stdH = np.std(self.allTunesH, axis=1)
        stdH[stdH==0] += 1e-10
        meanV = np.mean(self.allTunesV, axis=1)
        stdV = np.std(self.allTunesV, axis=1)
        stdV[stdV==0] += 1e-10

        polyH = curve_fit(self.curve, self.DP_s, meanH, p0=[0.2,6,0], absolute_sigma=False, sigma=1/stdH**2)
        polyV = curve_fit(self.curve, self.DP_s, meanV, p0=[0.33,6,0], absolute_sigma=False, sigma=1/stdV**2)

        self.measQH = polyH[0][0]
        self.measQpH = polyH[0][1]
        self.measQppH = polyH[0][2]

        self.measQV = polyV[0][0]
        self.measQpV = polyV[0][1]
        self.measQppV = polyV[0][2]

        self.errMeasQH = polyH[1][0,0]
        self.errMeasQpH = polyH[1][1,1]
        self.errMeasQppH = polyH[1][2,2]

        self.errMeasQV = polyV[1][0,0]
        self.errMeasQpV = polyV[1][1,1]
        self.errMeasQppV = polyV[1][2,2]

    def launchScan(self):
        '''
        The main function that runs the scans within 
        the parameters that were specified.
        '''
        ts = time.time()
        self.measTime = datetime.datetime.utcfromtimestamp(ts+2*3600).strftime('%Y-%m-%d %H:%M:%S')

        self.LoadRFFrequencyFromDevice()    #Load the RF frequency of the master oscillator
        self.PushStoredRFToDB()             #Save this number in the database
        self.set_state(DevState.RUNNING)    
        self.makeDPArray()                  #Make the DP array and store the values



        #Check to make sure the step size isn't larger than Qs/2
        if np.diff(self.DP_s)[0] > 2e-3:
            self.set_status('Strongly recommend using smaller step size! \n' +
                            'Current step size: {:f}*1e-3 \n'.format(1000*np.diff(self.DP_s)[0]) +
                            'Make step size less than Qs/2 which is ~2e-3 \n' + 
                            'In fact I will set to FAULT as this is too serious to be allowed.')
            self.set_state(DevState.FAULT)
        else:

            self.allTunesH = np.zeros((self.Num_Steps, self.Acqs_PerStep)) #Final results arrays
            self.allTunesV = np.zeros((self.Num_Steps, self.Acqs_PerStep))

            try: #Generic error catching, if something goes wrong then error and set to fault.
                for i, dp in enumerate(self.DP_s):
                    if not self._stop_event.is_set(): #Check at each loop for a stop event flag
                        self.setFrequency(self.RFFreq_Device, dp) #Set each frequency
                        self.currentDF = self.RFFreq_Device.read_attribute('Frequency').value - self.storeRF*1e6 #Update delta F
                        self.set_status('Step {:d}/{:d} \n'.format(i+1,len(self.DP_s)) + 
                                        'dp/p = {:.2f}e-3'.format(dp*1e3)) #Make nice status
                        time.sleep(self.delay)
                        for j, st in enumerate(np.arange(self.Acqs_PerStep)):
                            time.sleep(self.delay/2)
                            self.allTunesH[i,j] = self.getTune(self.Q_Device, 'h') #Get tune values and load into array
                            time.sleep(self.delay/2)
                            self.allTunesV[i,j] = self.getTune(self.Q_Device, 'v')
                    else:
                        self.set_status('Aborted! Throwing a ValueError to break free!')
                        raise ValueError('Quitting')  
                
                self.set_status('Scan finished! Setting state to ON!')
                self.set_state(DevState.ON)
                self.QH_ave = np.mean(self.allTunesH,axis=1) #Set the averages just to provide a check
                self.QV_ave = np.mean(self.allTunesV,axis=1)
                self.doFit()
            except ValueError:
                self.set_status('Aborting scan. Resetting RF frequency and setting to ON!') 
                self.setFrequency(self.RFFreq_Device, 0) #return to correct rf frequency   
                self.currentDF = self.RFFreq_Device.read_attribute('Frequency').value - self.storeRF*1e6  
                self.set_state(DevState.ON)
            except:
                self.set_status('Errored for some reason! \n' +  
                                'Setting state to FAULT! \n' +
                                'Is there still beam? Is the tune measurement OK? \n' + 
                                'Resetting RF frequency!')
                self.setFrequency(self.RFFreq_Device, 0) #return to correct rf frequency   
                self.currentDF = self.RFFreq_Device.read_attribute('Frequency').value - self.storeRF*1e6 
                self.set_state(DevState.FAULT)
            
        
        self._stop_event.clear()    


    # PROTECTED REGION END #    //  ChromaMeasurement.class_variable

    # ----------------
    # Class Properties
    # ----------------

    Stored_RF_Property = class_property(
        dtype='double',
    )

    # -----------------
    # Device Properties
    # -----------------

    RFFrequency_Device = device_property(
        dtype='str', default_value="srrf/master-oscillator/1"
    )

    Tune_Device = device_property(
        dtype='str', default_value="srdiag/beam-tune/adjust"
    )

    Results_Device = device_property(
        dtype='str', default_value="srdiag/beam-chromaticity/measured"
    )

    # ----------
    # Attributes
    # ----------

    MinDP = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        unit="[1e-4]",
        display_unit="[1e-4]",
    )

    MaxDP = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        unit="[1e-4]",
        display_unit="[1e-4]",
    )

    NumSteps = attribute(
        dtype='int16',
        access=AttrWriteType.READ_WRITE,
    )

    AcqsPerStep = attribute(
        dtype='int16',
        access=AttrWriteType.READ_WRITE,
    )

    Delta_Frequency = attribute(
        dtype='double',
        unit="[Hz]",
        display_unit="[Hz]",
        format="%6.3f",
    )

    Stored_RF = attribute(
        dtype='float',
        access=AttrWriteType.READ_WRITE,
        unit="[MHz]",
        display_unit="[MHz]",
        format="%6.8f",
        memorized=True,
    )

    Meas_Q_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    Meas_Qp_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    Meas_Qpp_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    Meas_Q_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    Meas_Qp_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    Meas_Qpp_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
    )

    Delay = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    MeasurementTime = attribute(
        dtype='str',
        access=AttrWriteType.READ_WRITE,
    )

    errMeas_Q_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    errMeas_Qp_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    errMeas_Qpp_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    errMeas_Q_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    errMeas_Qp_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    errMeas_Qpp_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        display_level=DispLevel.EXPERT,
    )

    DPS = attribute(
        dtype=('double',),
        max_dim_x=51,
    )

    TunesH_Averaged = attribute(
        dtype=('double',),
        max_dim_x=101,
    )

    TunesV_Averaged = attribute(
        dtype=('double',),
        max_dim_x=101,
    )

    TunesH = attribute(
        dtype=(('double',),),
        max_dim_x=101, max_dim_y=101,
        display_level=DispLevel.EXPERT,
    )

    TunesV = attribute(
        dtype=(('double',),),
        max_dim_x=101, max_dim_y=101,
        display_level=DispLevel.EXPERT,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(ChromaMeasurement.init_device) ENABLED START #
        util = PyTango.Util.instance()
        self.db = util.get_database()


        self.On()
        self.energy = 6e9
        self.momCompac = 8.5112e-05
        self.m_e = 511e3
        self.gamma = (self.energy- self.m_e)/self.m_e
        self.gamma_t = np.sqrt(1/self.momCompac)
        self.freq_2_p = (1/self.gamma**2) - (1/self.gamma_t**2)

        self.RFFreq_Device = PyTango.DeviceProxy(self.RFFrequency_Device)
        self.Q_Device = PyTango.DeviceProxy(self.Tune_Device)
        self.Meas_Device = PyTango.DeviceProxy(self.Results_Device)

        #self.storeRFFrequency(self.RFFreq_Device)
        #self.storeRF = 352.37215778

        self.storeRF = self.RFFreq_Device.read_attribute('Frequency').value/1e6 if self.LoadStoredRFFromDB() == '' else self.LoadStoredRFFromDB()

        self.delay = 6
        if float(self.RFFreq_Device.read_attribute('Frequency').value)/1e6 != float(self.LoadStoredRFFromDB()):
            self.set_status("WARNING. Stored RF device property does not match current RF frequency! \n" +
                            "Please figure out what you want to do. \n" + 
                            "Setting state to FAULT in the meantime. See below for hints on next step.\n" +
                            "If this is first time on, check Stored RF Frequency and, if OK, PushRFFrequencyToDevice \n" +
                            "Or you should figure out which RF Frequency is the good one and then either LoadRFFrequencyFromDevice or PushRFFrequencyToDevice! \n" + 
                            "Then RESET!")
            self.set_state(DevState.FAULT)



        self.Min_DP = -2
        self.Max_DP = 2
        self.Num_Steps = 5
        self.Acqs_PerStep = 2

        self._stop_event = Event()
        # PROTECTED REGION END #    //  ChromaMeasurement.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(ChromaMeasurement.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  ChromaMeasurement.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(ChromaMeasurement.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  ChromaMeasurement.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_MinDP(self):
        # PROTECTED REGION ID(ChromaMeasurement.MinDP_read) ENABLED START #
        return self.Min_DP
        # PROTECTED REGION END #    //  ChromaMeasurement.MinDP_read

    def write_MinDP(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.MinDP_write) ENABLED START #
        self.Min_DP = value
        # PROTECTED REGION END #    //  ChromaMeasurement.MinDP_write

    def is_MinDP_allowed(self, attr):
        # PROTECTED REGION ID(ChromaMeasurement.is_MinDP_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_MinDP_allowed

    def read_MaxDP(self):
        # PROTECTED REGION ID(ChromaMeasurement.MaxDP_read) ENABLED START #
        return self.Max_DP
        # PROTECTED REGION END #    //  ChromaMeasurement.MaxDP_read

    def write_MaxDP(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.MaxDP_write) ENABLED START #
        self.Max_DP = value
        # PROTECTED REGION END #    //  ChromaMeasurement.MaxDP_write

    def is_MaxDP_allowed(self, attr):
        # PROTECTED REGION ID(ChromaMeasurement.is_MaxDP_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_MaxDP_allowed

    def read_NumSteps(self):
        # PROTECTED REGION ID(ChromaMeasurement.NumSteps_read) ENABLED START #
        return self.Num_Steps
        # PROTECTED REGION END #    //  ChromaMeasurement.NumSteps_read

    def write_NumSteps(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.NumSteps_write) ENABLED START #
        self.Num_Steps = value
        # PROTECTED REGION END #    //  ChromaMeasurement.NumSteps_write

    def read_AcqsPerStep(self):
        # PROTECTED REGION ID(ChromaMeasurement.AcqsPerStep_read) ENABLED START #
        return self.Acqs_PerStep
        # PROTECTED REGION END #    //  ChromaMeasurement.AcqsPerStep_read

    def write_AcqsPerStep(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.AcqsPerStep_write) ENABLED START #
        self.Acqs_PerStep = value
        # PROTECTED REGION END #    //  ChromaMeasurement.AcqsPerStep_write

    def is_AcqsPerStep_allowed(self, attr):
        # PROTECTED REGION ID(ChromaMeasurement.is_AcqsPerStep_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_AcqsPerStep_allowed

    def read_Delta_Frequency(self):
        # PROTECTED REGION ID(ChromaMeasurement.Delta_Frequency_read) ENABLED START #
        return self.currentDF
        # PROTECTED REGION END #    //  ChromaMeasurement.Delta_Frequency_read

    def read_Stored_RF(self):
        # PROTECTED REGION ID(ChromaMeasurement.Stored_RF_read) ENABLED START #
        return self.storeRF
        # PROTECTED REGION END #    //  ChromaMeasurement.Stored_RF_read

    def write_Stored_RF(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Stored_RF_write) ENABLED START #
        pass
        # PROTECTED REGION END #    //  ChromaMeasurement.Stored_RF_write

    def read_Meas_Q_H(self):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Q_H_read) ENABLED START #
        return self.measQH
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Q_H_read

    def write_Meas_Q_H(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Q_H_write) ENABLED START #
        self.measQH = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Q_H_write

    def read_Meas_Qp_H(self):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qp_H_read) ENABLED START #
        return self.measQpH
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qp_H_read

    def write_Meas_Qp_H(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qp_H_write) ENABLED START #
        self.measQpH = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qp_H_write

    def read_Meas_Qpp_H(self):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qpp_H_read) ENABLED START #
        return self.measQppH
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qpp_H_read

    def write_Meas_Qpp_H(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qpp_H_write) ENABLED START #
        self.measQppH = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qpp_H_write

    def read_Meas_Q_V(self):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Q_V_read) ENABLED START #
        return self.measQV
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Q_V_read

    def write_Meas_Q_V(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Q_V_write) ENABLED START #
        self.measQV = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Q_V_write

    def read_Meas_Qp_V(self):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qp_V_read) ENABLED START #
        return self.measQpV
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qp_V_read

    def write_Meas_Qp_V(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qp_V_write) ENABLED START #
        self.measQpV = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qp_V_write

    def read_Meas_Qpp_V(self):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qpp_V_read) ENABLED START #
        return self.measQppV
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qpp_V_read

    def write_Meas_Qpp_V(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Meas_Qpp_V_write) ENABLED START #
        self.measQppV = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Meas_Qpp_V_write

    def read_Delay(self):
        # PROTECTED REGION ID(ChromaMeasurement.Delay_read) ENABLED START #
        return self.delay
        # PROTECTED REGION END #    //  ChromaMeasurement.Delay_read

    def write_Delay(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.Delay_write) ENABLED START #
        self.delay = value
        # PROTECTED REGION END #    //  ChromaMeasurement.Delay_write

    def read_MeasurementTime(self):
        # PROTECTED REGION ID(ChromaMeasurement.MeasurementTime_read) ENABLED START #
        return self.measTime
        # PROTECTED REGION END #    //  ChromaMeasurement.MeasurementTime_read

    def write_MeasurementTime(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.MeasurementTime_write) ENABLED START #
        self.measTime = value
        # PROTECTED REGION END #    //  ChromaMeasurement.MeasurementTime_write

    def read_errMeas_Q_H(self):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Q_H_read) ENABLED START #
        return self.errMeasQH
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Q_H_read

    def write_errMeas_Q_H(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Q_H_write) ENABLED START #
        self.errMeasQH = value
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Q_H_write

    def read_errMeas_Qp_H(self):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qp_H_read) ENABLED START #
        return self.errMeasQpH
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qp_H_read

    def write_errMeas_Qp_H(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qp_H_write) ENABLED START #
        self.errMeasQpH = value
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qp_H_write

    def read_errMeas_Qpp_H(self):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qpp_H_read) ENABLED START #
        return self.errMeasQppH
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qpp_H_read

    def write_errMeas_Qpp_H(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qpp_H_write) ENABLED START #
        self.errMeasQppH = value
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qpp_H_write

    def read_errMeas_Q_V(self):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Q_V_read) ENABLED START #
        return self.errMeasQV
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Q_V_read

    def write_errMeas_Q_V(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Q_V_write) ENABLED START #
        self.errMeasQV = value
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Q_V_write

    def read_errMeas_Qp_V(self):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qp_V_read) ENABLED START #
        return self.errMeasQpV
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qp_V_read

    def write_errMeas_Qp_V(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qp_V_write) ENABLED START #
        self.errMeasQpV = value
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qp_V_write

    def read_errMeas_Qpp_V(self):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qpp_V_read) ENABLED START #
        return self.errMeasQppV
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qpp_V_read

    def write_errMeas_Qpp_V(self, value):
        # PROTECTED REGION ID(ChromaMeasurement.errMeas_Qpp_V_write) ENABLED START #
        self.errMeasQppV = value
        # PROTECTED REGION END #    //  ChromaMeasurement.errMeas_Qpp_V_write

    def read_DPS(self):
        # PROTECTED REGION ID(ChromaMeasurement.DPS_read) ENABLED START #
        return self.DP_s
        # PROTECTED REGION END #    //  ChromaMeasurement.DPS_read

    def read_TunesH_Averaged(self):
        # PROTECTED REGION ID(ChromaMeasurement.TunesH_Averaged_read) ENABLED START #
        return self.QH_ave
        # PROTECTED REGION END #    //  ChromaMeasurement.TunesH_Averaged_read

    def read_TunesV_Averaged(self):
        # PROTECTED REGION ID(ChromaMeasurement.TunesV_Averaged_read) ENABLED START #
        return self.QV_ave
        # PROTECTED REGION END #    //  ChromaMeasurement.TunesV_Averaged_read

    def read_TunesH(self):
        # PROTECTED REGION ID(ChromaMeasurement.TunesH_read) ENABLED START #
        return self.allTunesH
        # PROTECTED REGION END #    //  ChromaMeasurement.TunesH_read

    def is_TunesH_allowed(self, attr):
        # PROTECTED REGION ID(ChromaMeasurement.is_TunesH_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return self.get_state() not in [DevState.RUNNING]
        else:
            return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_TunesH_allowed

    def read_TunesV(self):
        # PROTECTED REGION ID(ChromaMeasurement.TunesV_read) ENABLED START #
        return self.allTunesV
        # PROTECTED REGION END #    //  ChromaMeasurement.TunesV_read


    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def LoadStoredRFFromDB(self):
        # PROTECTED REGION ID(ChromaMeasurement.LoadStoredRFFromDB) ENABLED START #
        val = self.db.get_device_property(self.get_name(), 'Stored_RF')
        return float(val['Stored_RF'][0])
        # PROTECTED REGION END #    //  ChromaMeasurement.LoadStoredRFFromDB

    def is_LoadStoredRFFromDB_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_LoadStoredRFFromDB_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_LoadStoredRFFromDB_allowed

    @command(
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(ChromaMeasurement.On) ENABLED START #
        self.set_state(DevState.ON)
        # PROTECTED REGION END #    //  ChromaMeasurement.On

    def is_On_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_On_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_On_allowed

    @command(
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(ChromaMeasurement.Off) ENABLED START #
        self.set_state(DevState.OFF)
        # PROTECTED REGION END #    //  ChromaMeasurement.Off

    def is_Off_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_Off_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_Off_allowed

    @command(
    )
    @DebugIt()
    def Scan(self):
        # PROTECTED REGION ID(ChromaMeasurement.Scan) ENABLED START #

        # start am optimization Timer (not a thread, to execute .cancel() on all running timers on delete of the object)
        partialFuncforTimer = partial(self.launchScan)
        self.tmr = Thread(target = partialFuncforTimer)

        #self.tmr.isDaemon()# in case server is killed the thread is killed too
        self.tmr.start()

        # PROTECTED REGION END #    //  ChromaMeasurement.Scan

    def is_Scan_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_Scan_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_Scan_allowed

    @command(
    )
    @DebugIt()
    def AbortScan(self):
        # PROTECTED REGION ID(ChromaMeasurement.AbortScan) ENABLED START #
        self._stop_event.set()
        # PROTECTED REGION END #    //  ChromaMeasurement.AbortScan

    def is_AbortScan_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_AbortScan_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON,DevState.OFF,DevState.FAULT]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_AbortScan_allowed

    @command(
    )
    @DebugIt()
    def PushStoredRFToDB(self):
        # PROTECTED REGION ID(ChromaMeasurement.PushStoredRFToDB) ENABLED START #
        self.db.put_device_property(self.get_name(), dict(Stored_RF=self.storeRF))
        # PROTECTED REGION END #    //  ChromaMeasurement.PushStoredRFToDB

    def is_PushStoredRFToDB_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_PushStoredRFToDB_allowed) ENABLED START #
        return self.get_state() not in [DevState.OFF,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_PushStoredRFToDB_allowed

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(ChromaMeasurement.Reset) ENABLED START #
        self.set_state(DevState.OFF)
        self.set_state(DevState.ON)
        # PROTECTED REGION END #    //  ChromaMeasurement.Reset

    def is_Reset_allowed(self):
        # PROTECTED REGION ID(ChromaMeasurement.is_Reset_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON,DevState.OFF,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaMeasurement.is_Reset_allowed

    @command(
    )
    @DebugIt()
    def LoadRFFrequencyFromDevice(self):
        # PROTECTED REGION ID(ChromaMeasurement.LoadRFFrequencyFromDevice) ENABLED START #
        self.storeRF = self.RFFreq_Device.read_attribute('Frequency').value/1e6
        # PROTECTED REGION END #    //  ChromaMeasurement.LoadRFFrequencyFromDevice

    @command(
    )
    @DebugIt()
    def PushRFFrequencyToDevice(self):
        # PROTECTED REGION ID(ChromaMeasurement.PushRFFrequencyToDevice) ENABLED START #
        self.RFFreq_Device.write_attribute('Frequency', self.storeRF*1e6)
        # PROTECTED REGION END #    //  ChromaMeasurement.PushRFFrequencyToDevice

    @command(
    )
    @DebugIt()
    def SendQP_Value(self):
        # PROTECTED REGION ID(ChromaMeasurement.SendQP_Value) ENABLED START #
        self.Meas_Device.write_attribute('Measurement_Time', self.measTime)
        self.Meas_Device.write_attribute('Qp_H', self.measQpH)
        self.Meas_Device.write_attribute('Qp_V', self.measQpV)
        # PROTECTED REGION END #    //  ChromaMeasurement.SendQP_Value

    @command(
    )
    @DebugIt()
    def SendQPP_Value(self):
        # PROTECTED REGION ID(ChromaMeasurement.SendQPP_Value) ENABLED START #
        self.Meas_Device.write_attribute('Measurement_Time', self.measTime)
        self.Meas_Device.write_attribute('Qpp_H', self.measQppH)
        self.Meas_Device.write_attribute('Qpp_V', self.measQppV)
        # PROTECTED REGION END #    //  ChromaMeasurement.SendQPP_Value

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(ChromaMeasurement.main) ENABLED START #
    return run((ChromaMeasurement,), args=args, **kwargs)
    # PROTECTED REGION END #    //  ChromaMeasurement.main

if __name__ == '__main__':
    main()
