from __future__ import print_function
import tango, time, datetime, os, sys
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib as mpl


def curve(dp, Q, Qp, Qpp):
    return Q+Qp*dp+0.5*Qpp*dp*dp

if __name__=='__main__':

    device = tango.DeviceProxy("srdiag/beam-chroma/ChromaScan")

    device.write_attribute('NumSteps', 11)
    device.write_attribute('AcqsPerStep', 4)
    device.write_attribute('MinDP', -2) #in units of 1e-4
    device.write_attribute('MaxDP', 2) #in units of 1e-4

    device.Scan()

    ts = time.time()
    timeNow = datetime.datetime.utcfromtimestamp(ts+2*3600).strftime('%Y-%m-%d %H:%M:%S')

    time.sleep(5)
    while True:
        ds = str(device.State())
        if 'RUNNING' in ds:
            pass
        elif 'FAULT' in ds:
            print('Device Server in fault, ditch it!')
            exit()
        elif 'ON' in ds:
            print('Scan finished!')
            break
        time.sleep(2)

    try:
        allTunesH = device.read_attribute('TunesH').value
        allTunesV = device.read_attribute('TunesV').value
        dps = device.read_attribute('DPS').value
    except:
        print("If this failed, it's possible the device server broke somehow and didn't set the values")
        exit()

    resample = np.arange(np.amin(dps), np.amax(dps), (np.amax(dps)-np.amin(dps))/1000) #for later




    
    meanH = np.mean(allTunesH, axis=1)
    stdH = np.std(allTunesH, axis=1)
    stdH[stdH==0] += 1e-10
    meanV = np.mean(allTunesV, axis=1)
    stdV = np.std(allTunesV, axis=1)
    stdV[stdV==0] += 1e-10

    polyH = curve_fit(curve, dps, meanH, p0=[0.2,6,0], absolute_sigma=False, sigma=1/stdH**2)
    polyV = curve_fit(curve, dps, meanV, p0=[0.33,6,0], absolute_sigma=False, sigma=1/stdV**2)


    #From here on it is just plotting
    fig, ax = plt.subplots(2,1,figsize=(8,8), sharex=True)

    ax[0].errorbar(1000*dps, meanH, yerr=stdH, color='r', linestyle='None')
    ax[0].plot(1000*dps, meanH, color='r', marker='o', linestyle='None')
    ax[0].plot(1000*resample, curve(resample, polyH[0][0], polyH[0][1], polyH[0][2]), color='b', linestyle='solid')

    ax[1].errorbar(1000*dps, meanV, yerr=stdV, color='r',linestyle='None')
    ax[1].plot(1000*dps, meanV, color='r', marker='o', linestyle='None')
    ax[1].plot(1000*resample, curve(resample, polyV[0][0], polyV[0][1], polyV[0][2]), color='b', linestyle='solid')

    #Make plot look good

    rect = fig.patch
    rect.set_facecolor('white') 
    plt.setp(ax[0].get_xticklabels(), visible=False)

    ax[0].set_ylabel(r'$Q_{H}$')
    ax[1].set_ylabel(r'$Q_{V}$')
    ax[1].set_xlabel(r'$dp/p\ [\mathrm{10^{-3}}]$')

    upax0 = np.amax(allTunesH)
    downax0 = np.amin(allTunesH)
    upax1 = np.amax(allTunesV)
    downax1 = np.amin(allTunesV)

    strH = r'$Q={:.3f} \pm {:.3f}$'.format(polyH[0][0], polyH[1][0,0]) +'\n'+ r"$Q'={:.3f} \pm {:.3f}$".format(polyH[0][1], polyH[1][1,1]) +'\n'+ r"$Q''={:.3f} \pm {:.3f}$".format(polyH[0][2], polyH[1][2,2])
    strV = r'$Q={:.3f} \pm {:.3f}$'.format(polyV[0][0], polyV[1][0,0]) +'\n'+ r"$Q'={:.3f} \pm {:.3f}$".format(polyV[0][1], polyV[1][1,1]) +'\n'+ r"$Q''={:.3f} \pm {:.3f}$".format(polyV[0][2], polyV[1][2,2])
    
    #Define position of text based on whether or not the chromaticity is +ve or -ve
    if polyH[0][1] > 0:
        ax[0].text(-1000*0.8*np.amax(dps), 0.6*(upax0-downax0) + downax0, strH)
    else:
        ax[0].text(1000*0.4*np.amax(dps), 0.6*(upax0-downax0) + downax0, strH)

    if polyV[0][1] > 0:
        ax[1].text(-1000*0.8*np.amax(dps), 0.6*(upax1-downax1) + downax1, strV)
    else:
        ax[1].text(1000*0.4*np.amax(dps), 0.6*(upax1-downax1) + downax1, strV)
    ax[0].set_xlim(np.amin(dps)*1.1*1000, np.amax(dps)*1.1*1000)
    ax[0].set_title('Chromaticity Measurement: {:s}'.format(timeNow))
    fig.subplots_adjust(left=0.15)
    #plt.savefig('./chromaticity_getTunes.png')
    plt.show()



    deviceSource = tango.DeviceProxy("srdiag/beam-chroma/measured")


    print('\n\n\n\n################################################# \n')
    print('Time of Measurement {:s}'.format(timeNow))
    print('Qp_H = {:.3f} +- {:.3f}'.format(polyH[0][1], polyH[1][1,1]))
    print('Qp_V = {:.3f} +- {:.3f}'.format(polyV[0][1], polyV[1][1,1]))
    print('\n################################################# \n\n')
    while True:
        qr = raw_input('Do you want to send this measurement to the Device Server (Y/N)? \n')
        qr = qr.lower()
        if 'n' in qr or 'y' in qr:
            break
        else:
            print('Please answer Yes or No you idiot!')
    if qr[0] == 'y':
        deviceSource.write_attribute('Qp_H', polyH[0][1])
        deviceSource.write_attribute('Qp_V', polyV[0][1])
        deviceSource.write_attribute('Measurement_Time', timeNow)
    elif qr[0] == 'n':
        print('Will not send!')



