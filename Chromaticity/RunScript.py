from __future__ import print_function
import tango, time, datetime, os, sys
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib as mpl


def curve(dp, Q, Qp, Qpp):
    return Q+Qp*dp+0.5*Qpp*dp*dp

if __name__=='__main__':

    device = tango.DeviceProxy("srdiag/beam-chromaticity/ChromaScan")

    device.write_attribute('NumSteps', 11)
    device.write_attribute('AcqsPerStep', 2)
    device.write_attribute('MinDP', -4) #in units of 1e-4
    device.write_attribute('MaxDP', 4) #in units of 1e-4
    device.write_attribute('Delay', 7) #in units of 1e-4
    device.Scan()


    time.sleep(5)
    while True:
        ds = str(device.State())
        if 'RUNNING' in ds:
            pass
        elif 'FAULT' in ds:
            print('Device Server in fault, ditch it!')
            exit()
        elif 'ON' in ds:
            print('Scan finished!')
            break
        time.sleep(2)

    try:
        allTunesH = device.read_attribute('TunesH').value
        allTunesV = device.read_attribute('TunesV').value
        dps = device.read_attribute('DPS').value
        QH = device.read_attribute('Meas_Q_H').value
        QV = device.read_attribute('Meas_Q_V').value
        QpH = device.read_attribute('Meas_Qp_H').value
        QpV = device.read_attribute('Meas_Qp_V').value
        QppH = device.read_attribute('Meas_Qpp_H').value
        QppV = device.read_attribute('Meas_Qpp_V').value

        errQH = device.read_attribute('errMeas_Q_H').value
        errQV = device.read_attribute('errMeas_Q_V').value
        errQpH = device.read_attribute('errMeas_Qp_H').value
        errQpV = device.read_attribute('errMeas_Qp_V').value
        errQppH = device.read_attribute('errMeas_Qpp_H').value
        errQppV = device.read_attribute('errMeas_Qpp_V').value

    except:
        print("If this failed, it's possible the device server broke somehow and didn't set the values")
        exit()

    timeNow = device.read_attribute('MeasurementTime').value
    
    resample = np.linspace(np.amin(dps), np.amax(dps), 1001)


    meanH = np.nanmean(allTunesH, axis=1)
    meanV = np.nanmean(allTunesV, axis=1)
    stdH = np.nanstd(allTunesH, axis=1)
    stdH[stdH==0] += 1e-10
    stdV = np.nanstd(allTunesV, axis=1)
    stdV[stdV==0] += 1e-10

    #From here on it is just plotting
    fig, ax = plt.subplots(2,1,figsize=(8,8), sharex=True)

    ax[0].errorbar(1000*dps, meanH, yerr=stdH, color='r', linestyle='None')
    ax[0].plot(1000*dps, meanH, color='r', marker='o', linestyle='None')
    ax[0].plot(1000*resample, curve(resample, QH, QpH, QppH), color='b', linestyle='solid')

    ax[1].errorbar(1000*dps, meanV, yerr=stdV, color='r',linestyle='None')
    ax[1].plot(1000*dps, meanV, color='r', marker='o', linestyle='None')
    ax[1].plot(1000*resample, curve(resample, QV, QpV, QppV), color='b', linestyle='solid')

    #Make plot look good

    rect = fig.patch
    rect.set_facecolor('white') 
    plt.setp(ax[0].get_xticklabels(), visible=False)

    ax[0].set_ylabel(r'$Q_{H}$')
    ax[1].set_ylabel(r'$Q_{V}$')
    ax[1].set_xlabel(r'$dp/p\ [\mathrm{10^{-3}}]$')

    upax0 = np.amax(allTunesH)
    downax0 = np.amin(allTunesH)
    upax1 = np.amax(allTunesV)
    downax1 = np.amin(allTunesV)

    strH = r'$Q={:.3f} \pm {:.3f}$'.format(QH, errQH) +'\n'+ r"$Q'={:.3f} \pm {:.3f}$".format(QpH, errQpH) +'\n'+ r"$Q''={:.3f} \pm {:.3f}$".format(QppH, errQppH)
    strV = r'$Q={:.3f} \pm {:.3f}$'.format(QV, errQV) +'\n'+ r"$Q'={:.3f} \pm {:.3f}$".format(QpV, errQpV) +'\n'+ r"$Q''={:.3f} \pm {:.3f}$".format(QppV,errQppV)
    
    #Define position of text based on whether or not the chromaticity is +ve or -ve
    if QpH > 0:
        ax[0].text(-1000*0.8*np.amax(dps), 0.6*(upax0-downax0) + downax0, strH)
    else:
        ax[0].text(1000*0.4*np.amax(dps), 0.6*(upax0-downax0) + downax0, strH)

    if QpV > 0:
        ax[1].text(-1000*0.8*np.amax(dps), 0.6*(upax1-downax1) + downax1, strV)
    else:
        ax[1].text(1000*0.4*np.amax(dps), 0.6*(upax1-downax1) + downax1, strV)
    ax[0].set_xlim(np.amin(dps)*1.1*1000, np.amax(dps)*1.1*1000)
    ax[0].set_title('Chromaticity Measurement: {:s}'.format(timeNow))
    fig.subplots_adjust(left=0.15)
    #plt.savefig('./chromaticity_getTunes.png')
    plt.show()



    print('\n\n\n\n################################################# \n')
    print('Time of Measurement {:s}'.format(timeNow))
    print('Qp_H = {:.3f} +- {:.3f}'.format(QpH, errQpH))
    print('Qp_V = {:.3f} +- {:.3f}'.format(QpV, errQpV))
    print('\n################################################# \n\n')
    while True:
        qr = raw_input('Do you want to send this measurement to the Device Server (Y/N)? \n')
        qr = qr.lower()
        if 'n' in qr or 'y' in qr:
            break
        else:
            print('Please answer Yes or No you idiot!')
    if qr[0] == 'y':
        device.SendQP_Value()
    elif qr[0] == 'n':
        print('Will not send!')


    print('\n\n\n\n################################################# \n')
    print('Time of Measurement {:s}'.format(timeNow))
    print('Qpp_H = {:.3f} +- {:.3f}'.format(QppH, errQppH))
    print('Qpp_V = {:.3f} +- {:.3f}'.format(QppV, errQppV))
    print('\n################################################# \n\n')
    while True:
        qr = raw_input('Do you want to send this measurement to the Device Server (Y/N)? \n')
        qr = qr.lower()
        if 'n' in qr or 'y' in qr:
            break
        else:
            print('Please answer Yes or No you idiot!')
    if qr[0] == 'y':
        device.SendQPP_Value()
    elif qr[0] == 'n':
        print('Will not send!')





