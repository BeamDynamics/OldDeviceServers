# -*- coding: utf-8 -*-
#
# This file is part of the ChromaSource project
#
# Copyright (C): 2019
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" measured-chromaticity

A device server to receive measured chromaticity values in order to be corrected by the adjust-chroma server.
"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(ChromaSource.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  ChromaSource.additionnal_import

__all__ = ["ChromaSource", "main"]


class ChromaSource(Device):
    """
    A device server to receive measured chromaticity values in order to be corrected by the adjust-chroma server.
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(ChromaSource.class_variable) ENABLED START #

    # PROTECTED REGION END #    //  ChromaSource.class_variable

    # ----------
    # Attributes
    # ----------

    Qp_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        label="Q'_H",
        memorized=True,
    )

    Qp_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        label="Q'_V",
        memorized=True,
    )

    Measurement_Time = attribute(
        dtype='str',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    Qpp_H = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        label="Q''_H",
        memorized=True,
    )

    Qpp_V = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        label="Q''_V",
        memorized=True,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(ChromaSource.init_device) ENABLED START #
        self.On()
        self.write_Measurement_Time('No Measurement')
        #self.write_Qp_H()
        #self.write_Qp_V()
        # PROTECTED REGION END #    //  ChromaSource.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(ChromaSource.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  ChromaSource.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(ChromaSource.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  ChromaSource.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_Qp_H(self):
        # PROTECTED REGION ID(ChromaSource.Qp_H_read) ENABLED START #
        return self.Chroma_H
        # PROTECTED REGION END #    //  ChromaSource.Qp_H_read

    def write_Qp_H(self, value):
        # PROTECTED REGION ID(ChromaSource.Qp_H_write) ENABLED START #
        self.Chroma_H = value
        # PROTECTED REGION END #    //  ChromaSource.Qp_H_write

    def is_Qp_H_allowed(self, attr):
        # PROTECTED REGION ID(ChromaSource.is_Qp_H_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaSource.is_Qp_H_allowed

    def read_Qp_V(self):
        # PROTECTED REGION ID(ChromaSource.Qp_V_read) ENABLED START #
        return self.Chroma_V
        # PROTECTED REGION END #    //  ChromaSource.Qp_V_read

    def write_Qp_V(self, value):
        # PROTECTED REGION ID(ChromaSource.Qp_V_write) ENABLED START #
        self.Chroma_V = value
        # PROTECTED REGION END #    //  ChromaSource.Qp_V_write

    def is_Qp_V_allowed(self, attr):
        # PROTECTED REGION ID(ChromaSource.is_Qp_V_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaSource.is_Qp_V_allowed

    def read_Measurement_Time(self):
        # PROTECTED REGION ID(ChromaSource.Measurement_Time_read) ENABLED START #
        return self.meas_time
        # PROTECTED REGION END #    //  ChromaSource.Measurement_Time_read

    def write_Measurement_Time(self, value):
        # PROTECTED REGION ID(ChromaSource.Measurement_Time_write) ENABLED START #
        self.meas_time = value
        # PROTECTED REGION END #    //  ChromaSource.Measurement_Time_write

    def read_Qpp_H(self):
        # PROTECTED REGION ID(ChromaSource.Qpp_H_read) ENABLED START #
        return self.ChromaPrime_H
        # PROTECTED REGION END #    //  ChromaSource.Qpp_H_read

    def write_Qpp_H(self, value):
        # PROTECTED REGION ID(ChromaSource.Qpp_H_write) ENABLED START #
        self.ChromaPrime_H = value
        # PROTECTED REGION END #    //  ChromaSource.Qpp_H_write

    def is_Qpp_H_allowed(self, attr):
        # PROTECTED REGION ID(ChromaSource.is_Qpp_H_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaSource.is_Qpp_H_allowed

    def read_Qpp_V(self):
        # PROTECTED REGION ID(ChromaSource.Qpp_V_read) ENABLED START #
        return self.ChromaPrime_V
        # PROTECTED REGION END #    //  ChromaSource.Qpp_V_read

    def write_Qpp_V(self, value):
        # PROTECTED REGION ID(ChromaSource.Qpp_V_write) ENABLED START #
        self.ChromaPrime_V = value
        # PROTECTED REGION END #    //  ChromaSource.Qpp_V_write

    def is_Qpp_V_allowed(self, attr):
        # PROTECTED REGION ID(ChromaSource.is_Qpp_V_allowed) ENABLED START #
        if attr==attr.READ_REQ:
            return True
        else:
            return self.get_state() not in [DevState.OFF,DevState.FAULT,DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaSource.is_Qpp_V_allowed


    # --------
    # Commands
    # --------

    @command(
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(ChromaSource.On) ENABLED START #
        # set state On
        self.set_state(DevState.ON)
        # PROTECTED REGION END #    //  ChromaSource.On

    def is_On_allowed(self):
        # PROTECTED REGION ID(ChromaSource.is_On_allowed) ENABLED START #
        return self.get_state() not in [DevState.ON]
        # PROTECTED REGION END #    //  ChromaSource.is_On_allowed

    @command(
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(ChromaSource.Off) ENABLED START #
        self.set_state(DevState.OFF)
        # PROTECTED REGION END #    //  ChromaSource.Off

    def is_Off_allowed(self):
        # PROTECTED REGION ID(ChromaSource.is_Off_allowed) ENABLED START #
        return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaSource.is_Off_allowed

    @command(
    )
    @DebugIt()
    def Reset(self):
        # PROTECTED REGION ID(ChromaSource.Reset) ENABLED START #
        pass
        # PROTECTED REGION END #    //  ChromaSource.Reset

    def is_Reset_allowed(self):
        # PROTECTED REGION ID(ChromaSource.is_Reset_allowed) ENABLED START #
        return self.get_state() not in [DevState.RUNNING]
        # PROTECTED REGION END #    //  ChromaSource.is_Reset_allowed

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(ChromaSource.main) ENABLED START #
    return run((ChromaSource,), args=args, **kwargs)
    # PROTECTED REGION END #    //  ChromaSource.main

if __name__ == '__main__':
    main()
